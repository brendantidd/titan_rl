import matplotlib.pyplot as plt
# plt.switch_backend('agg')
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import numpy as np
import seaborn as sns
import pandas as pd
import json
import argparse
from glob import glob
import os
from collections import deque
from pathlib import Path
home = str(Path.home())

def cur_num():
    path = home + '/Downloads/decay_gaps2'
    # json_files = [pos_json for pos_json in os.listdir(path) if pos_json.endswith('.json')]
    # for jfile in json_files:
    json_list = []
    milestones = [1,2,3,4,5,6,7]
    milestone = np.array(milestones)
    sigmoid = np.zeros(len(milestones))
    linear = np.zeros(len(milestones))
    exp = np.zeros(len(milestones))
    for jfile in glob(path + '/*.json'):
        # df['Milestone Number'] = np.array([1,2,3])


        if 'results_dists' in jfile:
            # print(jfile)
            read_json = pd.read_json(jfile)
            # print(df[2][0])
            # print(jfile.split('-tag')[0][-1])
            # print(jfile, read_json)
            decay = jfile.split('_')[2]
            # print(decay)
            tag = int(jfile.split('-tag')[0][-1]) - 1
            # if tag not in milestones:
            #     print(tag)
            #     continue
            # print(tag, read_json[2][0])
            if 'sigmoid' in decay:
                sigmoid[tag] = read_json[2][0]
            if 'linear' in decay:
                linear[tag] = read_json[2][0]
            if 'exp' in decay:
                exp[tag] = read_json[2][0]
                # print(sigmoid)
            # if 'linear' in decay:    
            #     df['Linear'][tag] = read_json[2][0]
            # if 'exp' in decay:    
            #     df['Exponential'][tag] = read_json[2][0]
            # print(df)
            # print(read_json[2])
            # df['Decay Type'] = 'Sigmoid'

            # if 'sigmoid' in jfile:
                # df['Decay Type'] = 'Sigmoid'
                # df['Decay Type'] = 0
            # if 'linear' in jfile:
                # df['Decay Type'] = 'Linear'
                # df['Decay Type'] = 1
            # if 'exp' in jfile:
                # df['Decay Type'] = 'Exponential'
                # df['Decay Type'] = 2
            # json_list.append(df)
    # df = pd.DataFrame()
    
    # data = pd.concat(json_list)
    df = pd.DataFrame({'Milestone Number':milestone,'Sigmoid':sigmoid,'Linear':linear, 'Exponential':exp})
    # print(new_data)
    # data = pd.DataFrame(new_data)
    # print(sigmoid)
    # data_preproc = pd.DataFrame({
    #     'Steps': x, 
    #     'Linear': 400-(x*32.5),
    #     'Exponential': 400*0.75**x ,
    #     # 'Sigmoid': 400*(1 - 1/(1+np.exp(-(x - 8))))})
    #     'Sigmoid': 400*(1 - 1/(1+np.exp(-(x - 6))))})

    print(df)
    melted = pd.melt(df, ['Milestone Number'], var_name='Decay Type', value_name='Distance')
    print(melted)
    # sns.relplot(x='Milestone Number', y='Distance', hue='Decay Type', data=pd.melt(df, ['Milestone Number'], var_name='Decay Type', value_name='Distance'),    kind="line", marker="o", legend=False)
    sns.relplot(x='Milestone Number', y='Distance', hue='Decay Type', data=pd.melt(df, ['Milestone Number'], var_name='Decay Type', value_name='Distance'),    kind="line", marker="o")
    plt.show()
    plt.savefig('plots/cur_num.png')

    # print(json_files)    # for me this prints ['foo.json']
    # montreal_json = pd.DataFrame.from_dict(json_files[0])
    # print(montreal_json)
    # print montreal_json['features'][0]['geometry']

    # jsons = []
    # for f_name in glob(path + '/*.json'):
    #     # df = pd.read_json('./Test/test.json')
    #     js = json.load('./Test/test.json')
    #     jsons.append(js)


    # paths = {'jumps':'jumps.json'}
    # data = {}
    # for name in paths:
    #     df = pd.read_json('./Test/test.json')
    #     print(df)
    #     data[name] = json.load(paths[name])
    #     x =    '{ "name":"John", "age":30, "city":"New York"}'

    #     print(data)
    # x = np.array([_ for _ in range(13)])
    # data_preproc = pd.DataFrame({
    #     'Steps': x, 
    #     'Linear': 400-(x*32.5),
    #     'Exponential': 400*0.75**x ,
    #     # 'Sigmoid': 400*(1 - 1/(1+np.exp(-(x - 8))))})
    #     'Sigmoid': 400*(1 - 1/(1+np.exp(-(x - 6))))})

    # # sns.lineplot(x='Year', y='value', hue='variable', data=pd.melt(data_preproc, ['Year']))
    # sns.relplot(x='Steps', y='Gain', hue='variable', data=pd.melt(data_preproc, ['Steps'], value_name='Gain'),    kind="line", marker="o", legend=False)
    # plt.savefig('plots/cur_num.png')

def multi_test2():
    fig, ax = plt.subplots()

    path = './json/run-jumps_ca-tag-Eval_dists.json'
    j = pd.read_json(path)
    data = np.array(j[2])
    df = pd.DataFrame({'Training Steps': np.array([_ for _ in range(len(data))])*50e7/len(data), 'y':data})
        
    # # print(df['ours_'])
    # # for i, data in enumerate(df):
    # for i, v in df.items():
    #     # print(i, v)
    #     # if key == 'Training Steps':
    #         # continue
    #     # data = df[key]
    #     # print(len(v))
    #     # idx1 =    df.index[:10]
    #     # df.loc[idx1, i].plot(ax=ax, color='green', label='')
    #     df.loc[:, v].plot(ax=ax, color='green', label='')
    #     # idx2 =    df.index[int(0.5*len(v)):int(0.75*len(v))]
    #     # df.loc[idx2, i].plot(ax=ax, color='orange', label='')
    df.loc[df.index[int(0.75*len(data)):], 'y'].plot(ax=ax, color='red', label='')

    # plt.text(x[-1], y[-1], 'sample {i}'.format(i=i))
    
    plt.show()

def smooth_data(data, window=50):
    slider = deque(maxlen=window)
    new_data = []
    for i in data:
        slider.append(i)
        new_data.append(np.mean(slider))
    return new_data

def reward_single(args):
    '''
    Single plot of reward. Terray increase, force decay, disturbance increase
    '''
    path = '/home/brendan/Pictures/titan/tensorboard/'
    max_data_len = 0
    # names = ['no_aux', 'aux', 'super', 'rew', 'rew_aux']
    # names = ['super', 'aux', 'rew_aux']
    names = ['new']
    # legend_names = ['No Aux', 'Aux', 'Supervised', 'Reward', 'Reward Aux']
    # legend_names = ['Supervised Learning', 'Reinforcement Learning']
    legend_names = ['Reinforcement Learning']
    # palette = sns.xkcd_palette(["windows blue", "amber", "dusty purple",    "faded green", "pale red" ][:len(names)])
    # palette = sns.xkcd_palette(["amber","windows blue", "pale red", "dusty purple",    "faded green", "pale red" ][:len(names)])
    palette = sns.xkcd_palette(["pale red"])
    # palette = sns.xkcd_palette(["pale red", "windows blue"])

    # legend_names = ['Constant', 'Original', 'Torque Matching', 'Target Value','Advantage Weighted Target Value']
    # seeds = ['21', '42', '84']
    df = {"Reward Choice":[], "Success":[], "Training Steps":[]}
    
    position_of_name = 3
    print(path)
    # data_length = 198
    data_length = 1000
    # total_length = 1e7
    # total_length = data_length * 16 * 300
    total_length = 3e6
    means = {x:0 for x in names}
    # for f_name in glob(path + '/*.json'):
    #     # print(f_name)
    # file_name =    f_name.split("/")[-1]
    for x, lx in zip(names, legend_names):
        f_name = path + "run-" + x + "-tag-success.json"
        # f_name = path + "run-" + x + "_" + s + "-tag-success.json"
        # print(f_name)
        j = pd.read_json(f_name)
        data = np.array(j[2])

        if args.smooth:
            data = smooth_data(data, window=args.window)
        # df[x][s] = data
        # print(len(data))
        data_length = len(data)
        print("data length", data_length)
        df["Reward Choice"].extend([lx for _ in range(data_length)])
        # df["Seed"].extend([s for _ in range(data_length)])
        df["Success"].extend(list(data)[:data_length])
        print(len(data), x)
        means[x] += data[-1]
        df["Training Steps"].extend([i*(total_length)/data_length for i in range(data_length)])
    # print("means", [(x, means[x]/3) for x in names])
    print("means", [means[x] for x in names])
    # print(df)

    df = pd.DataFrame(df)
        
    sns.set(font_scale=2.0)
    sns.set_style("whitegrid", {'axes.grid' : False})
    sns.lineplot(x="Training Steps", y="Success", hue="Reward Choice", data=df, palette=palette, legend=False)
    # plt.ticklabel_format(style='plain', axis='x')
    plt.ticklabel_format(style='scientific', scilimits=[0,0.5], axis='x')
    plt.ylim([-0.05, 1.0])
    plt.xlim([0, total_length])
    # plt.legend(legend_names,loc='best', frameon=False)
    plt.show()

def reward(args):
    '''
    Single plot of reward. Terray increase, force decay, disturbance increase
    '''
    path = '/home/brendan/Pictures/titan/tensorboard/'
    max_data_len = 0
    # names = ['no_aux', 'aux', 'super', 'rew', 'rew_aux']
    names = ['super', 'aux', 'rew_aux']
    # legend_names = ['No Aux', 'Aux', 'Supervised', 'Reward', 'Reward Aux']
    legend_names = ['Supervised Learning', 'Reinforcement Learning']
    # palette = sns.xkcd_palette(["windows blue", "amber", "dusty purple",    "faded green", "pale red" ][:len(names)])
    # palette = sns.xkcd_palette(["amber","windows blue", "pale red", "dusty purple",    "faded green", "pale red" ][:len(names)])
    palette = sns.xkcd_palette(["amber","windows blue"])
    # palette = sns.xkcd_palette(["pale red", "windows blue"])

    # legend_names = ['Constant', 'Original', 'Torque Matching', 'Target Value','Advantage Weighted Target Value']
    seeds = ['21', '42', '84']
    df = {"Reward Choice":[], "Success":[], "Seed":[], "Training Steps":[]}
    
    position_of_name = 3
    print(path)
    data_length = 198
    # total_length = 1e7
    # total_length = data_length * 16 * 300
    total_length = 1e6
    means = {x:0 for x in names}
    # for f_name in glob(path + '/*.json'):
    #     # print(f_name)
    # file_name =    f_name.split("/")[-1]
    for x, lx in zip(names, legend_names):
        for s in seeds:
            f_name = path + "run-" + x + "_" + s + "-tag-success.json"
            # print(f_name)
            j = pd.read_json(f_name)
            data = np.array(j[2])
            # print(file_name, x.lower(), s)
            # f_n = file_name.split("-")[1]
            # print(f_n)
            # f_name = "".join(f_n[:-3])
            # f_num = f_n[-2:]
            # print(f_name, f_num)
            # print(x.lower(), s)
            # print(x.lower() == f_name, s == f_num)
            
            # if x.lower() in file_name and s in file_n
            # if x.lower() in file_name and s in file_name:
            # if x.lower() in file_name and s == f_num:
            # if x.lower() == f_name and s == f_num:
            # print(x.lower(), s, file_name)
            # print(x)
            # print(data.shape)
            # all_data[x] = df[x] = data
            if args.smooth:
                data = smooth_data(data, window=args.window)
            # df[x][s] = data
            # print(len(data))
            df["Reward Choice"].extend([lx for _ in range(data_length)])
            df["Seed"].extend([s for _ in range(data_length)])
            df["Success"].extend(list(data)[:data_length])
            print(len(data), x, s)
            # if x == 'ours':
            #     mean_ours += data[-1]

            # if x == 'vpred':
            #     mean_value += data[-1]
            means[x] += data[-1]
            # print(data)
            # df["Training Steps"] = np.array([_ for _ in range(len(data))])
            # df["Training Steps"].extend([i*1e7/data_length for i in range(data_length)])
            df["Training Steps"].extend([i*(total_length)/data_length for i in range(data_length)])
            # print(len(df["Experiment"]), len(df["Seed"]), len(df["Success"]), len(df["Training Steps"]))
            
            # df[x] = pd.DataFrame({s:data})
            # df["Success"] = pd.Series(data)    

            # all_data["Success"] = data
            # print(x, len(data))
            # all_data[x] = df['Training Steps'] =    np.array([_ for _ in range(len(data))])*50e7/len(data)
            # break
    # print("our result:", mean_ours/3)
    # print("value result:", mean_value/3)
    print("means", [(x, means[x]/3) for x in names])
    # all_data["Training Steps"] = np.array([_ for _ in range(42)])*50e7/len(data)
    # df["Training Steps"] = np.array([_ for _ in range(42)])*1.5e7/42
    # df["Training Steps"] = np.array([_ for _ in range(42)])
    df = pd.DataFrame(df)

    # print(df)
    # exit()
    # sns.lineplot(x="Training Stepsnt", y="Success", hue="Reward", style="event", data=all_data)
    # sns.lineplot(x="Training Steps", y="Success", hue="Experiment", style="Seed", data=df)
    
    # sns.set(font_scale=1.25)
    sns.set(font_scale=2.0)
    # sns.color_palette("tab10")
    # sns.color_palette("hls", 8)
    # fig, ax = plt.subplots()
    sns.set_style("whitegrid", {'axes.grid' : False})
    # palette = sns.color_palette(["#4c72b0","#4c72b0","#55a868","#55a868","#c44e52"])
    # palette = sns.xkcd_palette(["windows blue", "amber", "dusty purple",    "faded green" ])
    # palette = sns.xkcd_palette(["windows blue", "amber"])
    # print(sns.color_palette())
    # sns.lineplot(x="Training Steps", y="Success", hue="Reward Choice", data=df, palette="tab10")
    # sns.lineplot(x="Training Steps", y="Success", hue="Reward Choice", data=df, palette="hls")
    # sns.lineplot(x="Training Steps", y="Success", hue="Reward Choice", data=df, palette=palette,ax=ax)
    sns.lineplot(x="Training Steps", y="Success", hue="Reward Choice", data=df, palette=palette, legend=False)
    # plt.ticklabel_format(style='plain', axis='x')
    plt.ticklabel_format(style='scientific', scilimits=[0,0.5], axis='x')
    plt.ylim([-0.05, 1.0])
    plt.xlim([0, total_length])
    # legend_names = ['Constant', 'Advantage Weighted Target Value','Original', 'Torque Matching', 'Target Value']
    # legend_names = ['Advantage Weighted Target Value','Constant','Target Torque' , 'Target Value','Original'] 
    # legend_names = ['No aux', 'aux'] 
    # plt.legend(legend_names,loc='best')
    # plt.legend(legend_names,loc='best', frameon=False)
    plt.legend(legend_names,loc='lower right', frameon=False)
    # leg = plt.legend(loc=[-0.02,0.5], frameon=False)

    # plt.gca().legend().set_title('')
    # leg.set_title('')
    # handles, labels = ax.get_legend_handles_labels()
    # ax.legend(handles=handles[1:], labels=labels[1:])
    # names = ['const', 'ours', 'rew', 'tor', 'vpred']
    plt.show()
    # exit()
    # neg_data = {}
    # for f_name in glob(path + 'negative/*.json'):
    #     print(f_name)
    #     j = pd.read_json(f_name)
    #     data = np.array(j[2])

    #     for x in names:
    #         # print(x.lower())
    #         if (x == 'Hurdles' and 'jump' in f_name) or x.lower() in f_name:
    #             # print(x)
    #             # print(data.shape)
    #             # all_data[x] = df[x] = data0
    #             data = smooth_data(data)
    #             neg_data[x] = pd.DataFrame({x:data})
    #             # print(x, len(data))
    #             # all_data[x] = df['Training Steps'] =    np.array([_ for _ in range(len(data))])*50e7/len(data)
    #             break
                
    # all_data['Flat_x1']         = [int(95*1000/1250), int( 520*1000/1250), int(584*1000/1250)]
    # all_data['Gaps_x1']         = [int(100*1000/1500),int( 500*1000/1500), int(90*1000/1500)]
    # all_data['Hurdles_x1']    = [int(110*1000/1500),int(950*1000/1500), int(50*1000/1500)]
    # all_data['Stairs_x1']     = [int(95*1000/1500), int( 620*1000/1500), int(80*1000/1500)]
    # all_data['Steps_x1']        = [int(95*1000/1500), int(    520*1000/1500), int(100*1000/1500)]

        # break
    # print(df)
    # df['Training Steps'] =    np.array([_ for _ in range(len(data))])*50e7/len(data)
    
    # fig, ax = plt.subplots()
    # colours = {}
    # # print
    # # df['gaps'] = df['gaps'][0]*100

    # # colours[0]= np.array(['r' ,'g','b'])
    # # sns.set_palette("husl")
    # # cmap = np.array(sns.light_palette((260, 75, 60), input="husl"))
    # # cmap=cmap.reshape([-1,4])
    # # print(len(cmap))
    # # for num, (i, v) in enumerate(df.items()):
    # i = 'gaps'
    # num = 0
    # num_data = 1000
    # lasts = []
    # # for num, i in enumerate(names):
    # # print(all_data)
    # legend_elements = []
    # labels = []

    # legend_elements.append(Line2D([0], [0], color='k', lw=2, label='Line', linestyle=':'))
    # legend_elements.append(Line2D([0], [0], color='k', lw=2, label='Line', linestyle='-.'))
    # legend_elements.append(Line2D([0], [0], color='k', lw=2, label='Line', linestyle='--'))
    # legend_elements.append(Line2D([0], [0], color='k', lw=2, label='Line', linestyle='-'))
    # labels.append('Stage 1')
    # labels.append('Stage 2')
    # labels.append('Stage 3')
    # labels.append('No Curriculum')

    # # for num, i in enumerate(['Flat', 'Hurdles', 'Gaps', 'Stairs', 'Steps']):
    # for num, i in enumerate(names):
    #     cmap = np.array(sns.light_palette((num*60, 75, 60), input="husl"))
    #     # print(i)
    # # df.loc[:30, i].plot(ax=ax, color=[250, 250, 250 ], label='')
    #     try:
    #         # all_data[i].loc[:30, i].plot(ax=ax, color=cmap[-5]), label='')
    #         # print(i)
    #         x1, x2, x3 = all_data[i+'_x1']
    #         print(i, x1, x2)
    #         x3 = num_data

    #         # ax.plot(np.array([_ for _ in range(x1)])*50e7/num_data, all_data[i].loc[:x1-1, i], color=tuple(cmap[-4]), label='', linewidth=1.0)
    #         # ax.plot(np.array([_ for _ in range(x1, x2)])*50e7/(num_data), all_data[i].loc[x1:x2-1, i], color=tuple(cmap[-3]), label='', linewidth=2.0)
    #         # last, = ax.plot(np.array([_ for _ in range(x2, x3)])*50e7/(num_data), all_data[i].loc[x2:x3-1, i], color=tuple(cmap[-1]), label='', linewidth=4.0)
    #         # col = cmap[-4]
    #         # col[-1] = 0
    #         ax.plot(np.array([x1])*50e7/num_data, all_data[i].loc[x1, i], color=tuple(cmap[-1]), label='', linewidth=2.0, markersize=40, fillstyle=None, marker='o', markeredgecolor=tuple(cmap[-4]), markerfacecolor=(1, 1, 0, 0.0))
    #         ax.plot(np.array([x2])*50e7/num_data, all_data[i].loc[x2, i], color=tuple(cmap[-1]), label='', linewidth=2.0, markersize=40, fillstyle=None, marker='o', markeredgecolor=tuple(cmap[-2]), markerfacecolor=(1, 1, 0, 0.0))
            


    #         ax.plot(np.array([_ for _ in range(x1)])*50e7/num_data, all_data[i].loc[:x1-1, i], color=tuple(cmap[-4]), label='', linewidth=3.0, linestyle=':')
    #         ax.plot(np.array([_ for _ in range(x1, x2)])*50e7/(num_data), all_data[i].loc[x1:x2-1, i], color=tuple(cmap[-3]), label='', linewidth=3.0, linestyle='-.')
    #         last, = ax.plot(np.array([_ for _ in range(x2, x3)])*50e7/(num_data), all_data[i].loc[x2:x3-1, i], color=tuple(cmap[-1]), label='', linewidth=3.0, linestyle='--')

    #         last, = ax.plot(np.array([_ for _ in range(num_data)])*50e7/(num_data), neg_data[i].loc[:, i], color=tuple(cmap[-1]), label='', linewidth=2.0, linestyle='-')


    #         legend_elements.append(Line2D([0], [0], color=tuple(cmap[-1]), lw=4, label='Line'))
            
    #         labels.append(i)
    #         lasts.append(last)




    #         # last, = ax.plot(np.array([_ for _ in range(x3, x4)])*50e7/(num_data), all_data[i].loc[x3:x4-1, i], color=tuple(cmap[-1]), label='', linewidth=4.0)
    #         # last, = ax.plot(np.array([_ for _ in range(x3, x4)])*50e7/(num_data), all_data[i].loc[x3:x4-1, i], color=tuple(cmap[-1]), label='', linewidth=4.0)
    #         # ax.plot(np.array([_ for _ in range(all_data[x+'_x2'])])*50e7/all_data[x+'_x2'], all_data[i].loc[:all_data[x+'_x2']-1, i], color=tuple(cmap[-2]), label='')
    #         # ax.plot(np.array([_ for _ in range(all_data[x+'_x3'])])*50e7/all_data[x+'_x3'], all_data[i].loc[:all_data[x+'_x3']-1, i], color=tuple(cmap[-1]), label='')
    #         # all_data[i].loc[30:60, i].plot(ax=ax, color=tuple(cmap[-3]), label='')
    #         # all_data[i].loc[60:, i].plot(ax=ax, color=tuple(cmap[-1]), label='')
    #     except Exception as e:
    #         print("exception", e)
    # # ax.xlabel = "Timesteps"
    # # ax.ylabel = "Episode Return"
    # ax.set_xlabel("Timesteps")
    # ax.set_ylabel("Episode Reward")

    # # legend_elements = [Line2D([0], [0], color='b', lw=4, label='Line'),
    # #                                    Line2D([0], [0], marker='o', color='w', label='Scatter',
    # #                                                 markerfacecolor='g', markersize=15),
    # #                                    Patch(facecolor='orange', edgecolor='r',
    # #                                                label='Color Patch')]

    # # legend_elements = [Patch(facecolor='orange', edgecolor='r',label='Color Patch')]
    # # legend_elements = [Line2D([0], [0], color='b', lw=4, label='Line')]
    # # legend_elements = [ax.plot([0], [0], color='b', lw=4, label='Line')]

    # # plt.legend(legend_elements)

    # ax.legend(handles=legend_elements, labels=labels)
    # # plt.legend(lasts, names)
    # # print(lasts)
    # # plt.legend(['Flat', 'Steps', 'Stairs'])
    # # df.loc[:30, i].plot(ax=ax, color=colours[num][0], label='')
    # # df.loc[30:60, i].plot(ax=ax, color=colours[num][1], label='')
    # # thing = df.loc[60:, i].plot(ax=ax, color=colours[num][2], label='')

    # # colours[0]= np.array(['r' ,'g','b'])
    # # for num, (i,)v) in enumerate(all_data.items()):
    # #     # df.loc[:, 'ours_'].plot(ax=ax, color='green', label='')
    # #     num = 0
    #     # v.loc[:30, i].plot(ax=ax, color=colours[num][0], label='')
    #     # v.loc[30:60, i].plot(ax=ax, color=colours[num][1], label='')
    #     # thing = v.loc[60:, i].plot(ax=ax, color=colours[num][2], label='')

    # # ax = sns.relplot(x='Training Steps', y='Reward', hue='C', data=pd.melt(df, ['Training Steps'], var_name='C', value_name='Reward'),    kind="line", legend=False)
    # # ax = sns.relplot(x='Training Steps', y='Reward', hue='C', data=pd.melt(df, ['Training Steps'], var_name='C', value_name='Reward'),    kind="line", legend=False)
    # plt.show()


def compare():
    stage3 = False
    sns.set(style="whitegrid")

    for n in ['jumps', 'gaps', 'flat', 'steps', 'stairs']:
        # path =    './json/comparisons/' + n
        path =    './json/comparison_submitted/' + n
        max_data_len = 0
        df = pd.DataFrame({})
        # df = pd.Series({})
        # all_data = {}
        max_length = 0
        for f_name in glob(path + '/*.json'):
            j = pd.read_json(f_name)
            data = np.array(j[2])
            data = smooth_data(data, window=5)
            label = ''
            fname = f_name.split('-')
            for name in fname[1].split('_'):
                if name in ['no', 'stage1', 'stage2', 'exp', 'link', 'ours', 'stage3']:
                    label += name
                    label += '_'
            if len(data) > max_length:
                max_length = len(data)
            print(n, label, len(data))
            # df[label] = data
            # pri
            df[label] = pd.Series(data)    

        df['Training Steps'] =    pd.Series(np.array([_ for _ in range(max_length)])*50e7/max_length)

        # cols = df.columns.tolist()
        # cols = 
        # no_link_
        # no_stage1_stage2_
        # ours_
        # stage1_no_stage2_
        # no_exp_
        # no_stage1_no_stage2_
        
        # order = ['Training Steps', 'no_stage1_no_stage2_', 
        #                 'stage1_no_stage2_',
        #                 'no_stage1_stage2_',
        #                 'ours_',
        #                 'no_exp_',
        #                 'no_link_']
        if stage3:
            order = ['Training Steps', 'no_stage1_no_stage2_', 
                            'stage1_no_stage2_',
                            'no_stage1_stage2_',
                            'stage1_stage2_',
                            'stage1_stage2_stage3_']
        else:
            order = ['Training Steps', 'no_stage1_no_stage2_', 
                            'stage1_no_stage2_',
                            'no_stage1_stage2_',
                            'stage1_stage2_']
        # df = df[order][:min_length]
        df = df[order]
        # print(df)
        print(n)
        ax = sns.relplot(x='Training Steps', y='% of Total Distance', hue='C', data=pd.melt(df, ['Training Steps'], var_name='C', value_name='% of Total Distance'),    kind="line", legend=False)
        # plt.legend(['No Stage 1, No Stage 2',
        #                         'Stage 1, No Stage 2',
        #                         'No Stage 1, Stage 2',
        #                         'Stage 1, Stage 2',
        #                         'Stage 1, Stage 2, forces on CoM only',
        #                         'Stage 1, Stage 2, no link to target'], loc='upper left')
        if stage3:
            plt.legend(['No Stage 1, No Stage 2',
                                    'Stage 1, No Stage 2',
                                    'No Stage 1, Stage 2',
                                    'Stage 1, Stage 2',
                                    'Stage 1, Stage 2, Stage 3'], loc='upper left')
        else:
            plt.legend(['No Stage 1, No Stage 2',
                                    'Stage 1, No Stage 2',
                                    'No Stage 1, Stage 2',
                                    'Stage 1, Stage 2'], loc='upper left')
        plt.ylim([-0.05, 1.0])
        plt.xlim([0, 50e7])
        plt.show()
        # plt.savefig('plots/compare.png')
            # all_data[label] = pd.DataFrame({'Training Steps': np.array([_ for _ in range(len(data))])*50e7/len(data), label:data})
        
def multi_test():

    fig, ax = plt.subplots()

    path =    './json/comparisons'
    max_data_len = 0
    # df = pd.DataFrame({})
    # df = pd.Series({})
    all_data = {}
    for f_name in glob(path + '/*.json'):

        # path =    './json/run-jumps_ca-tag-Eval_dists.json'
        j = pd.read_json(f_name)
        data = np.array(j[2])
        label = ''
        fname = f_name.split('-')
        for name in fname[1].split('_'):
            if name in ['no', 'stage1', 'stage2', 'exp', 'link', 'ours']:
                label += name
                label += '_'
        all_data[label] = pd.DataFrame({'Training Steps': np.array([_ for _ in range(len(data))])*50e7/len(data), label:data})
        
        # # print(f_name, label)
        # # print(data.shape)
        # # print(df)
        # # df.loc[range(len(l)),'b'] = l
        # df[label] = data
        # if max_data_len < len(data):
        #     max_data_len = len(data)
    # df['Training Steps'] = np.array([_ for _ in range(len(data))])*50e7/len(data)
    # print(all_data)
    # colours = ['green', 'orange', 'red']
    colours = {}
    # for i in range(6):
    # rgba
    my_cmap = sns.light_palette("Navy", as_cmap=True)

    print(my_cmap)
    # N = 500
    # data1 = np.random.randn(N)
    # data2 = np.random.randn(N)
    # colors = np.linspace(0,1,N)
    # plt.scatter(data1, data2, c=colors, cmap=my_cmap)
    colours[0]= np.array(['r' ,'g','b'])
    for num, (i,v) in enumerate(all_data.items()):
        # df.loc[:, 'ours_'].plot(ax=ax, color='green', label='')
        num = 0
        v.loc[:30, i].plot(ax=ax, color=colours[num][0], label='')
        v.loc[30:60, i].plot(ax=ax, color=colours[num][1], label='')
        thing = v.loc[60:, i].plot(ax=ax, color=colours[num][2], label='')
        
        # v.loc[:30, i].plot(ax=ax, color=my_cmap[0], label='')
        # v.loc[30:60, i].plot(ax=ax, color=my_cmap[1], label='')
        # v.loc[60:, i].plot(ax=ax, color=my_cmap[2], label='')
    # fig.colorbar(line, ax=axs[1])
    # plt.colorbar(colours[0])
    # plt.colorbar(thing)

    # Colour shading for each line, legend that has colour shading in it

    plt.show()

    # print(df['ours_'])
    # for i, data in enumerate(df):
    # df.loc[:, 'ours_'].plot(ax=ax, color='green', label='')

    # for i, v in df.items():
        # print(i, v)
        # if key == 'Training Steps':
            # continue
        # data = df[key]
        # print(len(v))
        # idx1 =    df.index[:10]
        # df.loc[idx1, i].plot(ax=ax, color='green', label='')
        # df.loc[:, i].plot(ax=ax, color='green', label='')
        # idx2 =    df.index[int(0.5*len(v)):int(0.75*len(v))]
        # df.loc[idx2, i].plot(ax=ax, color='orange', label='')
        # df.loc[df.index[int(0.75*len(v)):], i].plot(ax=ax, color='red', label='')

    # plt.text(x[-1], y[-1], 'sample {i}'.format(i=i))

    # Display plot
    # plt.show()

def eval_decay():

    # path = home + '/Downloads/'
    path =    './json/run-jumps_ca-tag-Eval_dists.json'
    print("loading from ", path)
    j = pd.read_json(path)
    print(j)
    data = np.array(j[2])
    # df = pd.DataFrame({'x':np.array([_ for _ in range(len(data))])*50e7/len(data),'y':data})
    df = pd.DataFrame({'Training Steps':np.array([_ for _ in range(len(data))])*50e7/len(data),'y':data})
    # df = pd.DataFrame({'x':np.array([_ for _ in range(len(data))])*50e7/len(data),'y':data})

    # range_x = [-1, 0, 1, 2]
    range_x = [1,1]
    range_x2 = [1,1]
    range_y = [1, 0]
    # ax = sns.lineplot(x = range_x, y = range_y, markers = True)
    # ax.fill_between(range_x, range_y, facecolor = 'red', alpha = 0.5)
    # ax.fill_between(range_x, range_y, facecolor = 'green', alpha = 0.5)
    # sns.lineplot(ax = ax, x = [range_x[0], range_x[-1]], y = [0, 0], color = 'black')
    # sns.lineplot(ax = ax, x = [0, 0], y = [range_y[0], range_y[-1]], color = 'black')

    # ax.fill_between(range_x, range_y,[ax.get_ylim()[1]]*len(range_x), facecolor = 'red', alpha = 0.5)
    # ax.fill_between(range_x, range_y,[ax.get_ylim()[0]]*len(range_x), facecolor = 'green', alpha = 0.5)

    # sns.relplot(x='x', y='y', hue='y1', data=pd.melt(df, ['x'], var_name='y1', value_name='y'),    kind="line", marker="o")
    # sns.relplot(x='x', y='y', hue='y1', data=pd.melt(df, ['x'], var_name='y1', value_name='y'),    kind="line")
    # ax = sns.relplot(x='Training Steps', y='% of Total Distance', data=pd.melt(df, ['Training Steps'], value_name='% of Total Distance'),    kind="line")
    # ax = sns.lineplot(x='Training Steps', y='% of Total Distance', data=pd.melt(df, ['Training Steps'], value_name='% of Total Distance'),    kind="line")
    ax = sns.lineplot(x='Training Steps', y='% of Total Distance', data=pd.melt(df, ['Training Steps'], value_name='% of Total Distance'))
    range_x = df['Training Steps']
    range_x2 = df['Training Steps']
    range_y = 1
    ax.fill_betweenx(range_y,range_x, x2=range_x2, facecolor = 'red', alpha = 0.5)

    plt.show()
    # plt.savefig('plots/cur_num.png')

    # g = sns.FacetGrid(df, row='Filename', height=3.0, aspect=4) 
    # g = g.map(plt.plot, 'Time(min)', 'Smoothed Humidity') 
    # x = plt.gca().axes.get_xlim() 
    # for ax in g.axes: 
    #     ax[0].fill_between(x, y1=85, y2=90, color=sns.xkcd_rgb['green'], alpha=0.2)

def single():

    path = home + '/Downloads/'
    j = pd.read_json(path + 'run-hj_continuous-tag-rewards.json')
    buffer = deque(maxlen=100)
    data = np.array(j[2])
    r_data = []
    for i in data:
        buffer.append(i)
        r_data.append(np.mean(buffer))
        # print(i)
    
    df = pd.DataFrame({'x':[_ for _ in range(len(r_data))],'y1':r_data, 'y2':data})

    # sns.relplot(x='x', y='y', hue='y1', data=pd.melt(df, ['x'], var_name='y1', value_name='y'),    kind="line", marker="o")
    sns.relplot(x='x', y='y', hue='y1', data=pd.melt(df, ['x'], var_name='y1', value_name='y'),    kind="line")
    plt.show()
    # plt.savefig('plots/cur_num.png')

def exp_decay():
    x = np.array([_ for _ in range(20)])
    data = {'Steps': x}
    for rate in [0.65,0.75,0.8]:
        data['Exponential ' + str(rate)] = 400*(rate)**x 
        data['Exponential ' + str(rate)] = 400*(rate)**x 
        data['Exponential ' + str(rate)] = 400*(rate)**x 
    # data['cut_off'] = 10

    data_preproc = pd.DataFrame(data)
    # sns.lineplot(x='Year', y='value', hue='variable', data=pd.melt(data_preproc, ['Year']))
    sns.relplot(x='Steps', y='Gain', hue='Decay', data=pd.melt(data_preproc, ['Steps'], value_name='Gain', var_name='Decay'),    kind="line", marker="o", legend=False)
    # plt.legend(loc='upper left')
    plt.legend(loc='upper right')
    plt.ylim(0, 400)
    plt.xlim(0, 20)
    plt.savefig('plots/exp_decay.png')
    
    plt.show()

def decay_type():
    x = np.array([_ for _ in range(20)])
    data = {'Steps': x}
    for rate in [1,2,3]:
        data['Linear' + str(rate)]            = 400-(x*(50 - 7*(rate)))
        data['Exponential' + str(rate)] = 400*(0.60 + 0.07*rate)**x 
        data['Sigmoid' + str(rate)]         = 400*(1 - 1/(1+np.exp(-(x - 2.5*rate))))
    data_preproc = pd.DataFrame(data)
    # sns.lineplot(x='Year', y='value', hue='variable', data=pd.melt(data_preproc, ['Year']))
    # sns.relplot(x='Steps', y='Gain', hue='variable', data=pd.melt(data_preproc, ['Steps'], value_name='Gain'),    kind="line", marker="o", legend=False)
    ax = sns.relplot(x='Steps', y='Gain', hue='variable', data=pd.melt(data_preproc, ['Steps'], value_name='Gain'),    kind="line", marker="o")
    ax.get_axes()[0].legend(loc='lower left')
    plt.ylim(0, 400)
    plt.xlim(0, 20)
    
    # plt.show()
    plt.savefig('plots/decays.png')

def example1():
    num_rows = 20
    years = list(range(1990, 1990 + num_rows))
    data_preproc = pd.DataFrame({
        'Year': years, 
        'A': np.random.randn(num_rows).cumsum(),
        'B': np.random.randn(num_rows).cumsum(),
        'C': np.random.randn(num_rows).cumsum(),
        'D': np.random.randn(num_rows).cumsum()})

    # sns.lineplot(x='Year', y='value', hue='variable', data=pd.melt(data_preproc, ['Year']))

    sns.relplot(x='Year', y='value', hue='variable', data=pd.melt(data_preproc, ['Year']),    kind="line")

    plt.savefig('plots/output.png')

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--exp', default="decay_type")
    parser.add_argument('--window', default=10, type=int)
    parser.add_argument('--smooth', default=False, action="store_true")
    args = parser.parse_args()

    funcs = {'decay_type':decay_type,
                     'cur_num'     :cur_num,
                     'eval_decay' :         eval_decay,
                     'exp_decay' :exp_decay,
                     'multi_test' :multi_test,
                     'multi_test2' :multi_test2,
                     'reward' :reward,
                     'reward_single' :reward_single,
                     'compare' :compare,
                     'single'        :single}
    
    funcs[args.exp](args)