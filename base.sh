#!/bin/bash
+
mpirun -np 16 python run.py --seed 84 --exp $1_$2 --obstacle_type $1 $3

hostname

exit 0
EOT
