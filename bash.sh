#!/bin/bash
declare -a Arguments=(
                      # "--train_detector --seed 21 --add_reward --exp rew_21"
                      # "--train_detector --seed 42 --add_reward --exp rew_42"
                      # "--train_detector --seed 84 --add_reward --exp rew_84"
                      # "--train_detector --seed 42 --supervised --exp super_42"
                      # "--train_detector --seed 84 --supervised --exp super_84"
                      "--train_detector --seed 21 --add_reward --gap_aux --exp rew_aux_21"
                      "--train_detector --seed 42 --add_reward --gap_aux --exp rew_aux_42"
                      "--train_detector --seed 84 --add_reward --gap_aux --exp rew_aux_84"
                      )

for (( i=0; i<${#Arguments[@]}; i++ )); do 
  mpirun -np 16 --oversubscribe python3 titan_rl.py ${Arguments[$i]}
done