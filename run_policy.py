#! /usr/bin/env python3
import warnings
warnings.filterwarnings('ignore')
import scripts.utils as U
import scripts.mpi_utils as MPI_U
import argparse
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
import tensorboardX
from scripts import logger
import numpy as np
import random
from mpi4py import MPI
from pathlib import Path
home = str(Path.home())
import json
import git
from six.moves import shlex_quote
import rospy
np.set_printoptions(precision=3, suppress=True)
import time 
import cv2

def run(args):
    if args.train_detector:
        import matplotlib.pyplot as plt
        plt.switch_backend('agg')
        args.folder = 'detector'

    PATH = home + '/results/titan_rl/latest/' + args.folder + '/' + args.exp + '/'
    
    print("Currently running experiment: ", [i for i in vars(args) if getattr(args, i) == True], "with PATH: ", PATH)

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_workers = comm.Get_size()
    myseed = args.seed + 10000 * rank
    np.random.seed(myseed)
    random.seed(myseed)
    tf.compat.v1.set_random_seed(myseed)

    if not args.test_pol:
        logger.configure(dir=PATH)
        if rank == 0:
            writer = tensorboardX.SummaryWriter(log_dir=PATH)
            repo = git.Repo(search_parent_directories=True)
            sha = repo.head.object.hexsha
            with open(PATH + 'commandline_args.txt', 'w') as f:
              f.write('Hash:')
              f.write(str(sha) + "\n")
              json.dump(args.__dict__, f, indent=2)
            print('Save git commit and diff to {}/git.txt'.format(PATH))
            cmds = ["echo `git rev-parse HEAD` >> {}".format(
                            shlex_quote(os.path.join(PATH, 'git.txt'))),
                            "git diff >> {}".format(
                            shlex_quote(os.path.join(PATH, 'git.txt')))]
            os.system("\n".join(cmds))
        else: 
            writer = None 
    else: 
        writer = None 

    if args.train_detector:
        if args.single_wp:
            args.horizon = 200
        else:
            args.horizon = 300
    else:
        args.horizon = 180
    # 1000 updates 
    if args.train_detector:
        args.max_ts = 200*args.horizon*num_workers
        # args.max_ts = 1000*args.horizon*num_workers
    else:
        # args.max_ts = 1000*args.horizon*num_workers
        args.max_ts = 1200*args.horizon*num_workers
    if args.env == 'pb':
        from assets.env_pb import Env
    # elif args.env == 'gz':
    #     from assets.env_gz import Env
    # elif args.env == 'subt':
    else:
        from assets.env_gz import Env
        # args.horizon = 1000
        
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.02)
    # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.08)
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                            intra_op_parallelism_threads=1,             
                                            gpu_options=gpu_options), graph=None)

    if args.use_gaps or args.run_gaps:
        from models.gaps import GapDetector
        gap_detector = GapDetector(name="gaps", args=args, sess=sess)
        # gap_detector.load(home + '/results/titan_rl/latest/aux/' + args.exp + '/')
        # args.vis = False
        env = Env(PATH=PATH, args=args, rank=rank, gap_detector=gap_detector)
    else:
        env = Env(PATH=PATH, args=args, rank=rank)

    # args.horizon = 20
    if args.alg == 'sac':
        from models.sac import Model
        initial_steps = 0
        train_steps = int(args.horizon/4)
    elif args.alg == 'ppo':

        if args.use_gaps:
            # from models.ppo_gaps import Model
            from models.ppo import Model
        elif args.curl:
            from models.ppo_curl import Model
        elif args.aux or args.just_aux:
            from models.ppo_aux import Model
        else:
            from models.ppo import Model
        initial_steps = 0
        # horizon = 2048

    # if args.use_gaps:
    #     pol = Model(name=str(args.env), env=env, PATH=PATH, horizon=1024, ac_size=env.ac_size, ob_size=env.ob_size, im_size=[64], args=args, writer=writer, vis=args.vis, hid_size=args.hid_size, max_timesteps=args.max_ts, lr=3e-4, batch_size=64)
    # else:
    pol = Model(name="gap_behaviour", env=env, PATH=PATH, horizon=1024, ac_size=env.ac_size, ob_size=env.ob_size, im_size=env.im_size,args=args, writer=writer, vis=args.vis, hid_size=args.hid_size, max_timesteps=args.max_ts, lr=3e-4, batch_size=64)
    
    if args.train_detector:
        # if args.gap_aux:
        from models import ppo_gaps
        detect = ppo_gaps.Model(name="ppo_gap_detector", env=env, PATH=PATH, horizon=1024, ac_size=1, ob_size=env.detect_ob_size, im_size=env.im_size,args=args, writer=writer, vis=args.vis, hid_size=args.hid_size, max_timesteps=args.max_ts, lr=3e-4, batch_size=64)
        # else:
        #     detect = Model(name="ppo_gap_detector", env=env, PATH=PATH, horizon=1024, ac_size=1, ob_size=env.detect_ob_size, im_size=env.im_size,args=args, writer=writer, vis=args.vis, hid_size=args.hid_size, max_timesteps=args.max_ts, lr=3e-4, batch_size=64)


    # policies['training'].set_training_params(max_timesteps=max_timesteps, learning_rate=learning_rate, horizon=horizon)

    U.initialize_uninitialized()
    if args.alg == 'sac':
        sess.run(pol.target_init)
    MPI_U.sync_from_root(sess, pol.vars, comm=comm) 

    if args.use_gaps or args.run_gaps:
        # gap_detector.load(home + '/results/titan_rl/latest/aux/aux3/')
        gap_detector.load(home + '/results/titan_rl/latest/gaps/new_gaps4/')
        # gap_detector.load('./weights/gaps/')


    if args.test_pol or args.train_detector:
        try:
            # WEIGHTS_PATH = PATH 
            # pol.load(WEIGHTS_PATH)
            # pol.load('./weights/gaps_behaviour2/')
            pol.load('./weights/gaps_behaviour4/')
        except Exception as e:
            print(e)
            print("No weights to load")
            exit()

    rank = MPI.COMM_WORLD.Get_rank()
    prev_done = True
    ep_ret = 0
    ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_steps = 0
    if args.test_pol or args.train_detector:
        stochastic = False
    else:
        stochastic = True
    
    if args.env != 'pb' and rank == 0:
        env.pause_unpause(pause=False)
    ob, im = env.reset()
    if args.train_detector:
        detect_ob = env.detect_ob
        detect_ac_data = []
        gap = env.gaps

    
    # im = env.get_im()
    # if args.display_im:
    #     env.heightmap.display_hm(env.costmap, env.pos, env.yaw, env.initial_robot_pos, env.initial_yaw, world=env.heightmap.world, rank=env.rank)

    while not rospy.is_shutdown():
        # ====================================================================
        # Step env
        # ====================================================================    
        t1 = time.time()

        if args.train_detector:
            if detect.timesteps_so_far > detect.max_timesteps:
                break
            ac, _, _, _ = pol.step(ob, im, stochastic=False) 
            detect_ac, vpred, _, neglogp = detect.step(detect_ob, im, stochastic=True) 
            est_aux = [0,0,0,0,0]
            # detect_ac = [0]
            # print(detect_ac)
        else:
            if pol.timesteps_so_far > pol.max_timesteps:
                break
            detect_ac = None
            if args.alg == 'ppo':
                ac, vpred, _, neglogp = pol.step(ob, im, stochastic=stochastic) 
                if args.aux or args.just_aux:
                    est_aux = pol.get_aux(ob, im) 
                else:
                    est_aux = [0,0,0,0,0]
            elif args.alg == 'sac':
                ac = pol.step(ob, im, stochastic=stochastic) 
            
        # if args.env == 'subt' and ep_steps < 1000:
        #     ac = [0.5,0]
        # print(ac)
        next_ob, next_im, rew, done, _ = env.step(ac, est_aux, detect_ac)
        if args.train_detector:
            next_detect_ob = env.detect_ob
            next_gap = env.gaps

        if args.train_detector:
            ac = detect_ac
        # next_im = env.get_im(ep_steps)
        # cv2.imshow('frame', next_im)
        # cv2.waitKey(1)
        # print(time.time() - t1)
        if env.steps == args.horizon:
            done = True
        # ====================================================================
        # Training 
        # ====================================================================
        # print(ob.shape)
        if not args.test_pol:
            if args.train_detector:
                # if args.gap_aux:
                detect.add_to_buffer([detect_ob, im, gap, detect_ac, rew, done, vpred, neglogp])  
                # else:
                #     detect.add_to_buffer([detect_ob, im, detect_ac, rew, done, vpred, neglogp])  
                if ep_steps > initial_steps and ep_steps % args.horizon == 0:
                    env.publish_actions([0,0])
                    _, next_vpred, _, _ = detect.step(next_detect_ob, next_im, stochastic=True)
                    detect.run_train({'ep_rets':ep_rets, 'ep_lens':ep_lens}, last_value=next_vpred, last_done=done)
                    ep_rets = []
                    ep_lens = []
            else:
                if args.alg == 'ppo':
                    if args.aux or args.just_aux:
                        aux = env.get_aux()
                        # print(aux)
                        pol.add_to_buffer([ob, im, ac, rew, done, vpred, neglogp, aux])  
                    else:
                        pol.add_to_buffer([ob, im, ac, rew, done, vpred, neglogp])  
                    if ep_steps > initial_steps and ep_steps % args.horizon == 0:
                        # if args.env != 'pb' and rank == 0:
                        #     env.pause_unpause(pause=True)
                        # t2 = time.time()
                        # if args.env != 'pb':
                        env.publish_actions([0,0])
                        _, next_vpred, _, _ = pol.step(next_ob, next_im, stochastic=stochastic)
                        pol.run_train({'ep_rets':ep_rets, 'ep_lens':ep_lens}, last_value=next_vpred, last_done=done)
                        # if args.display_im:
                        #     env.heightmap.display_hm(env.costmap, env.pos, env.yaw, env.initial_robot_pos, env.initial_yaw, world=env.heightmap.world, rank=env.rank)

                        ep_rets = []
                        ep_lens = []
                        # print("training took:", time.time() - t2)
                        # if args.env != 'pb' and rank == 0:
                        #     env.pause_unpause(pause=False)
                elif args.alg == 'sac':
                    pol.add_to_buffer([ob, im, ac, rew, next_ob, next_im, done])
                    # if ep_steps > initial_steps and ep_steps % train_steps == 0:
                    if ep_steps > initial_steps and ep_steps % 1 == 0:
                        # if args.env != 'pb':
                            # env.publish_actions([0,0])
                        if ep_steps % args.horizon == 0:
                            # if args.env != 'pb' and rank == 0:
                            #     env.pause_unpause(pause=True)
                            pol.run_train(ep_rets, ep_lens, evaluate=True)
                            ep_rets = []
                            ep_lens = []
                            # if args.env != 'pb' and rank == 0:
                            #     env.pause_unpause(pause=False)
                        else:
                            # if args.env != 'pb':
                            #     env.publish_actions([0,0])
                            # if args.env != 'pb' and rank == 0:
                            #     env.pause_unpause(pause=True)
                            pol.run_train(ep_rets, ep_lens, evaluate=False)
                            # if args.env != 'pb' and rank == 0:
                            #     env.pause_unpause(pause=False)
                        env.last_training_step = env.steps

        prev_done = done
        ob = next_ob
        im = next_im
        if args.train_detector:
            detect_ob = next_detect_ob
            gap = next_gap
        ep_ret += rew
        ep_len += 1
        ep_steps += 1
        if args.train_detector:
            detect_ac_data.append(detect_ac[0] > 0.5)

        # print(env.gap, env.z)
        # ====================================================================
        # Done
        # ====================================================================
        if done:   
            ep_rets.append(ep_ret)  
            ep_lens.append(ep_len) 
            ep_ret = 0
            ep_len = 0   
            ob, im = env.reset()
            
            # im = env.get_im()
            if args.train_detector:
                if rank == 0 and env.episodes % 10 == 0:
                # if rank == 0:
                    num_axes = 2
                    fig, axes = plt.subplots(num_axes, figsize=(10, 3*num_axes))
                    axes[0].plot([_ for _ in range(len(detect_ac_data))], detect_ac_data, alpha=1.0)    
                    
                    
                    fig.tight_layout(pad=4.0)
                    plt.savefig(PATH + 'plot' + str(env.episodes) + str(env.pred) + '.png', bbox_inches='tight')
                    plt.close()
                detect_ob = env.detect_ob
                gap = env.gaps
                detect_ac_data = []
    # ===========================================================  

if __name__=="__main__":
    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument('--folder', default='titan')
    parser.add_argument('--difficulty', default=1, type=int)
    # parser.add_argument('--stacked', default=1, type=int)
    parser.add_argument('--stacked', default=5, type=int)
    parser.add_argument('--obstacle_type', default='high_jumps')
    parser.add_argument('--oct', default=True, action='store_false')
    parser.add_argument('--supervised', default=False, action='store_true')
    parser.add_argument('--gap_aux', default=False, action='store_true')
    parser.add_argument('--train_detector', default=False, action='store_true')
    parser.add_argument('--add_reward', default=False, action='store_true')
    parser.add_argument('--display_hm', default=False, action='store_true')
    parser.add_argument('--flat_e2e', default=False, action='store_true')
    parser.add_argument('--costmap', default=False, action='store_true')
    parser.add_argument('--single_wp', default=False, action='store_true')
    parser.add_argument('--label_bit', default=False, action='store_true')
    parser.add_argument('--just_aux', default=False, action='store_true')
    parser.add_argument('--aux_size', default=6, type=int)
    parser.add_argument('--aux', default=False, action='store_true')
    parser.add_argument('--use_gaps', default=False, action='store_true')
    parser.add_argument('--vis', default=True, action='store_false')
    # parser.add_argument('--run_gaps', default=True, action='store_false')
    parser.add_argument('--run_gaps', default=False, action='store_true')
    parser.add_argument('--curl', default=False, action='store_true')
    parser.add_argument('--augment', default=False, action='store_true')
    parser.add_argument('--record_step', default=True, action='store_false')
    parser.add_argument('--subt_hm', default=False, action='store_true')
    parser.add_argument('--individual', default=False, action='store_true')
    # parser.add_argument('--max_ts', default=3e6, type=int, help="max training time steps")
    parser.add_argument('--max_ts', default=5e6, type=int, help="max training time steps")
    parser.add_argument('--gaps_learning_rate', default=0.001, type=float)
    # parser.add_argument('--max_ts', default=5e6, type=int, help="max training time steps")
    # parser.add_argument('--max_ts', default=100e6, type=int, help="max training time steps")
    parser.add_argument('--horizon', default=128, type=int, help="episode length")
    parser.add_argument('--hid_size', default=256, type=int, help="hidden layer size")
    parser.add_argument('--env', default='gz', help="gz for Gazebo, pb for pybullet, subt to use the subt stack")
    parser.add_argument('--alg', default='ppo', help="ppo or sac")
    parser.add_argument('--robot', default='dynamic', help="pumpkin or dynamic")
    # parser.add_argument('--vis_type', default='depth', help="occ_map or depth")
    parser.add_argument('--vis_type', default='occ_map', help="occ_map or depth")
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)
