import numpy as np
import cv2

class OccupancyMap():
    """
    Creates an occupancy map from a list of boxes.
    box_info should be a list of boxes inserted into the simulation:
        - box_info = [box_id, positions, sizes, colours]
        where each element is a list e.g. box_id = [box_id1, box_id2]. Position is both [position + orientation] (euler orientation)
    sim_type = "gazebo" or "pybullet"
    For Pybullet size is the size of the boxes, 
    For Gazebo size is the half extents of the boxes
    """

    def __init__(self, box_info, im_size=[120, 120, 1], grid_size=0.1, initial_yaw=0, robot_pos=[0, 0, 0], args=None, rank=0, add_noise=False, sim_type="pybullet"):
        self.box_info = box_info
        self.im_size = im_size
        self.grid_size = grid_size
        self.initial_yaw = initial_yaw
        self.initial_robot_pos = robot_pos
        self.args = args
        self.rank = rank
        self.add_noise = add_noise
        self.sim_type = sim_type
        self.get_world_map()

    def get_world_map(self):
        """
        Create world map of the box locations (only call when the world changes).
        """
        self.boxes = np.ones([1, 3])
        # self.z_offset = None
        self.z_offset = 0

        # Go through all the boxes and create a grid array of the box size and then populate with its world cordinate frame values (int)
        # TODO Fix the truncation with float to int in trasnform_rot3d
        if self.box_info is not None:
            for pos, size in zip(self.box_info[1], self.box_info[2]):
                x, y, z = pos[0]/self.grid_size, pos[1]/self.grid_size, pos[2]
                if self.sim_type == "gazebo":
                    x_size, y_size, z_size = (
                        size[0]/2)/self.grid_size, (size[1]/2)/self.grid_size, size[2]/2
                elif self.sim_type == "pybullet":
                    x_size, y_size, z_size = (
                        size[0])/self.grid_size, (size[1])/self.grid_size, size[2]
                ij = np.array([[i, j, 0] for i in range(int(0-x_size), int(0+x_size) + 1)
                                for j in range(int(0-y_size), int(0+y_size) + 1) if i != 0 and j != 0])
                # ij = np.array([[i, j, z + z_size] for i in range(int(0-x_size), int(0+x_size))
                            #    for j in range(int(0-y_size), int(0+y_size))])
                if self.z_offset == None:
                    self.z_offset = z + z_size
                # print(x,y, len(ij))
                om_pts = self.transform_rot3d(pos[5], [x, y, 0], ij)
                om_pts = self.transform_rot3d(self.initial_yaw, [int(
                    self.initial_robot_pos[0]/self.grid_size), int(self.initial_robot_pos[1]/self.grid_size), 0], om_pts)
                # Store all the boxes' coordinates to be populated into the world map
                self.boxes = np.concatenate([self.boxes, om_pts], axis=0)

        self.boxes = self.boxes[1:, :]

        self.min_x, self.min_y, _ = np.min(self.boxes, axis=0)
        self.max_x, self.max_y, _ = np.max(self.boxes, axis=0)
        # Not used but handy if you want to know the real world size of the map
        self.map_extents = [self.min_x*self.grid_size, self.max_x *
                            self.grid_size, self.min_y*self.grid_size, self.max_y*self.grid_size]
        # Make sure when we are at the edge there is enough buffer for the image else out of bound error
        self.min_x = int(self.min_x - (self.im_size[0]/2))
        self.min_y = int(self.min_y - (self.im_size[1]/2))
        self.max_x = int(self.max_x + (self.im_size[0]/2))
        self.max_y = int(self.max_y + (self.im_size[1]/2))
        # Offset has the world is on the edge and not on the middle
        self.world_offset = np.array([self.min_x, self.min_y, 0])
        # This actually creates the map
        self.world = np.zeros(
            [self.max_x - self.min_x, self.max_y - self.min_y], dtype=np.float32)
        # print("display offset", self.world_offset)
        self.boxes = self.boxes - self.world_offset
        for x, y, z in zip(self.boxes[:, 0], self.boxes[:, 1], self.boxes[:, 2]):
            # Some hard coded 0.2m size as obstacle or not. No int val for z-axis
            self.world[int(x), int(y)] = z - self.z_offset > 0.2

        # Created now so that it can be populated later (initialised once). This is the actual height map output
        self.om_ij = np.array([[i, j] for i in range(
            0, self.im_size[0]) for j in range(0, self.im_size[1])])

    def get_om(self, robot_pos, robot_yaw=None):
        """
        Return an occupancy map of size at the robot location.
        Specify if it should rotated with the robot (by providing a robot yaw), else it will be fixed to an initial yaw (similar to the costmap in Subt)
        """
        # ij is the map around the robot as an image
        # ij = np.array([[i, j, 0] for i in range(int(0-self.im_size[0]/2), int(0+self.im_size[0]/2))
        #                for j in range(int(0-self.im_size[1]/2), int(0+self.im_size[1]/2))])
        ij = np.array([[i, j, 0] for i in range(int(0-self.im_size[0]/2), int(0+self.im_size[0]/2) + 1)
                       for j in range(int(0-self.im_size[1]/2), int(0+self.im_size[1]/2) + 1) if i != 0 and j != 0])
        x_pos, y_pos = robot_pos[0]/self.grid_size, robot_pos[1]/self.grid_size
        if robot_yaw == None:
            robot_yaw = self.initial_yaw
        else:
            robot_yaw = robot_yaw

        # Transform this map into the world map at the robot position
        om_pts = self.transform_rot3d(
            robot_yaw, [int(x_pos), int(y_pos), 0], ij)
        # Not sure why this was here?
        # om_pts = self.transform_rot3d(self.initial_yaw, [int(self.initial_robot_pos[0]/self.grid_size), int(self.initial_robot_pos[1]/self.grid_size), 0], om_pts)
        # Offset to centre of map
        om_pts = om_pts - self.world_offset
        self.om = np.ones(self.im_size)
        # Clip the map to be within the shape of the world
        om_pts[:, 0] = np.clip(om_pts[:, 0], 0, self.world.shape[0]-1)
        om_pts[:, 1] = np.clip(om_pts[:, 1], 0, self.world.shape[1]-1)
        # This only required if there is more than 1 channel for the height map
        self.om[self.om_ij[:, 0], self.om_ij[:, 1],
                0] = self.world[om_pts[:, 0], om_pts[:, 1]]

        # Add noise to the occupancy map
        if self.add_noise:
            # Select which noise_profile to test
            noise_profile = 0
            if noise_profile == 0:
                # Takes a kernel of around grid and has a 1% chance of being subjected to noise.
                white_idx = np.where(self.om == 1)
                noise_shape = 2
                for x, y in zip(white_idx[0], white_idx[1]):
                    if x > noise_shape and y > noise_shape and x < self.im_size[0] - noise_shape and y < self.im_size[0] - noise_shape:
                        if np.random.random() < 0.01:
                            self.om[x-noise_shape:x+noise_shape, y-noise_shape:y+noise_shape,
                                    0] = np.random.randint(0, 2, size=[noise_shape*2, noise_shape*2])
            elif noise_profile == 1:
                # Takes a kernel of around grid and has a 1% chance of being subjected to noise.
                white_idx = np.where(self.om == 1)
                noise_shape = 2
                for x, y in zip(white_idx[0], white_idx[1]):
                    if x > noise_shape and y > noise_shape and x < self.im_size[0] - noise_shape and y < self.im_size[0] - noise_shape:
                        if np.random.random() < 0.01:
                            self.om[x-noise_shape:x+noise_shape, y-noise_shape:y+noise_shape,
                                    0] = np.random.randint(0, 2, size=[noise_shape*2, noise_shape*2])

        # Displays in opencv. Can rescale to make image bigger
        if self.args.display_im:
            x_min = int(- self.im_size[0]/2)
            x_max = int(+ self.im_size[0]/2)
            y_min = int(- self.im_size[1]/2)
            y_max = int(+ self.im_size[1]/2)
            x1, y1, x2, y2, x3, y3, x4, y4 = x_max, y_max, x_min, y_max, x_min, y_min, x_max, y_min
            rect_pts = np.array(
                [[x1, y1, 0], [x2, y2, 0], [x3, y3, 0], [x4, y4, 0]])
            rect_pts = self.transform_rot3d(
                robot_yaw, [int(x_pos), int(y_pos), 0], rect_pts)
            rect_pts = self.transform_rot3d(self.initial_yaw, [int(
                self.initial_robot_pos[0]/self.grid_size), int(self.initial_robot_pos[1]/self.grid_size), 0], rect_pts)
            rect_pts = (rect_pts - self.world_offset)[:, :2]
            rect_pts = np.array([[rect_pts[0][1], rect_pts[0][0]], [rect_pts[1][1], rect_pts[1][0]], [
                                rect_pts[2][1], rect_pts[2][0]], [rect_pts[3][1], rect_pts[3][0]]])
            wm_col = cv2.cvtColor(self.world, cv2.COLOR_GRAY2RGB)
            cv2.polylines(wm_col, [rect_pts], True, (0, 0, 255), 2)
            if self.rank == 0:
                # width, height but shape is height width
                wm_colS = cv2.resize(wm_col, ( wm_col.shape[1] * 6, wm_col.shape[0] * 6 ))
                cv2.imshow('wm_col' + str(self.rank), wm_colS)
                cv2.waitKey(1)

            local = np.ones_like(wm_col)
            x1, x2, y1, y2 = int(local.shape[0]/2-self.om.shape[0]/2), int(local.shape[0]/2+self.om.shape[0]/2), int(
                local.shape[1]/2-self.om.shape[1]/2), int(local.shape[1]/2+self.om.shape[1]/2)
            local[x1:x2, y1:y2] = self.om
            localS = cv2.resize(local, ( local.shape[1] * 6, local.shape[0] * 6 ))
            cv2.imshow('im_col' + str(self.rank), localS)
            cv2.waitKey(1)

        return self.om

    def transform_rot3d(self, yaw, pos, points):
        rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), 0],
             [np.sin(yaw), np.cos(yaw), 0],
             [0, 0, 1]])
        # Round to nearest int and convert to int
        output = np.rint(np.add(np.dot(rot_mat, points.T).T, pos))
        return output.astype(np.int32)

    def transform_rot2d(self, yaw, pos, points):
        rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw)],
             [np.sin(yaw), np.cos(yaw)]])
        return np.add(np.dot(rot_mat, points.T).T, pos).astype(np.int32)

def to_int(input):
    return np.rint(input).astype(np.int32)

if __name__=="__main__":
    import time
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--display_im', default=True, action='store_false')
    # parser.add_argument('--display_im', default=False, action='store_true')
    args = parser.parse_args()
    dt = 0.1
    num_random_boxes = 3
    box_id = [0] * num_random_boxes
    # positions = [[np.random.uniform(-5, 5), np.random.uniform(-5, 5), 0, 0, 0, np.random.uniform(-1.5, 1.5)]
    #              for _ in range(num_random_boxes)]
    # sizes = [[np.random.uniform(0.2, 2),np.random.uniform(0.2, 2), 1] 
    #         for _ in range(num_random_boxes)]
    positions = [[i, i*2, 0, 0, 0, i]
                 for i in range(num_random_boxes)]
    sizes = [[1, 1.5, 1] 
            for _ in range(num_random_boxes)]
    box_info = [box_id, positions, sizes]
    print(box_info)
    om = OccupancyMap(box_info, args=args)
    speed = np.random.uniform(-1,1, size=2)
    yaw_speed = np.random.uniform(-1,1, size=1)
    pos = [0,0]
    yaw = 0

    while True:
        print("running loop")
        current_om = om.get_om(pos, yaw)
        t1 = time.time()
        while t1 - time.time() < dt:
            time.sleep(0.000001)
        pos += speed * dt
        yaw += yaw_speed * dt
        print("new pos and yaw", pos, yaw)
