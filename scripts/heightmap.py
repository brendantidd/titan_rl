import numpy as np
import cv2
import pybullet as p
import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

def get_world_map(box_info=None, world_shape=[30,10], grid_size=0.05):
    hm = np.zeros([int(world_shape[0]/grid_size), int(world_shape[1]/grid_size)]).astype(np.float32)
    # [boxIds, positions, sizes, frictions, colours]
    if box_info is not None:
        positions = box_info[1]
        sizes = box_info[2]
        for pos, size in zip(positions, sizes):
            x, y, z = int(pos[0]/grid_size),int(pos[1]/grid_size), int(pos[2]/grid_size) 
            x_size, y_size, z_size = int(size[0]/grid_size), int(size[1]/grid_size), int(size[2]/grid_size)
            yaw = pos[5]
            
            # points = np.arange(x_size+x * y_size+y).reshape([x_size+x, y_size+y])
            # rot_box = transform_rot(yaw, pos, points)

            for dx in range(x-x_size, x+x_size):
                for dy in range(y-y_size, y+y_size):
                    # hm_pts = transform_pts(hm.shape, np.array([dy, dx])) 
                    hm_pts = transform_rot(yaw, pos, np.array([dy, dx])) + 
                    print(hm_pts, np.array([dy, dx]))

                    hm[np.clip(hm_pts[1], 0, hm.shape[0]-1), np.clip(hm_pts[0], 0, hm.shape[1]-1)] = (z + z_size/grid_size)
    return hm

def get_map(hm, x, y, yaw):
    return 0

def transform_rot(yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw)],
            [np.sin(yaw), np.cos(yaw)]])
    # return np.dot(rot_mat,points.T).T.astype(np.int32)
    return np.add(np.dot(rot_mat,points.T).T,pos).astype(np.int32)

def get_line(x1,y1,x2,y2):
    m = (y1-y2)/(x1-x2)
    c = y1 - m*x1
    return m, c

def inside(x1,y1,x2,y2,x3,y3, px, py):
    AB = [x2-x1, y2-y1]
    AM = [px-x1, py-y1]
    BC = [x3-x2, y3-y2]
    BM = [px-x2, py-y2]
    part1 = np.dot(AB, AM)
    part2 = np.dot(BC, BM)
    part3 = np.dot(AB, AB)
    part4 = np.dot(BC, BC)
    print(part1, part2, part3, part4)
    return part1 > 0 and part1 <= part3 and part2 > 0 and part2 <= part4

def transform_pts(wm_size, arr):
    rot_mat = np.array(
        [[np.cos(np.pi/2), -np.sin(np.pi/2)],
        [np.sin(np.pi/2), np.cos(np.pi/2)]])
    arr =  np.dot(rot_mat,np.array(arr).T).T.astype(np.int32)
    if len(arr.shape)>1:
        new_arr = []
        for a in arr:
            new_arr.append([-1*a[1] + int(wm_size[1]/2),wm_size[0] + a[0]])
    else:
        new_arr = [-1*arr[1] + int(wm_size[1]/2),wm_size[0] + arr[0]]
    return np.array(new_arr)

if __name__=="__main__":
    from assets.obstacles import Obstacles
    ob = Obstacles()
    physicsClientId = p.connect(p.GUI)  
    p.loadMJCF("./assets/ground.xml")
    order = [np.random.choice(['flat', 'up', 'down'], p=[0.2,0.4,0.4]) for _ in range(10)]
    # order = ['up' for _ in range(10)]
    box_info = ob.up_down_flat(height_coeff=0.07, order=order)
    wm = get_world_map(box_info)
    dx_forward, dx_back, dy = 40, 20, 20
    x1, y1, x2, y2, x3, y3, x4, y4 = dx_forward, dy, -dx_back, dy, -dx_back, -dy, dx_forward, -dy
    # x,y,yaw = 70, -40, -0.4
    x,y,yaw = 50, 0,0.5
    norm_wm = wm/np.max(wm)
    wm_col = cv2.cvtColor(norm_wm,cv2.COLOR_GRAY2RGB)
    rect_pts = np.array([[y1,x1],[y2,x2],[y3,x3],[y4,x4]])
    pts = transform_rot(-1*yaw, [y,x], rect_pts)        
    pts = transform_pts(wm_col.shape, pts)
    cv2.polylines(wm_col, [pts], True, (0,255,0), 1)   
    test = np.ones(wm_col.shape)
    act = np.ones(wm_col.shape)
    # act = np.ones([dx_forward+dx_back, dy])
    for ii, i in enumerate(range(-dx_back, dx_forward)):
        for jj, j in enumerate(range(-dy, dy)):
            hm_pts = transform_rot(-1*yaw, [y, x], np.array([j, i]))
            hm_pts = transform_pts(wm.shape, hm_pts) 
            test[np.clip(hm_pts[1], 0, wm.shape[0]-1), np.clip(hm_pts[0], 0, wm.shape[1]-1), :] = norm_wm[np.clip(hm_pts[1], 0, wm.shape[0]-1), np.clip(hm_pts[0], 0, norm_wm.shape[1]-1)]
            
            # back_pts = transform_rot(-1*yaw, [y, x], np.array([j, i]))

            act[jj,ii] = norm_wm[np.clip(hm_pts[1], 0, wm.shape[0]-1), np.clip(hm_pts[0], 0, norm_wm.shape[1]-1)]
    cv2.imshow('frame',np.hstack([wm_col,test, act]))
    cv2.waitKey(0)