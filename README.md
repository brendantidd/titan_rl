# README #

## IROS notes ##
- Tag IROS2021
Load the robots:
`roslaunch titan_rl train_titan_rl`
Train policies
`mpirun -np 16 --oversubscribe python3 run_policy.py --exp test`
Train detector
`mpirun -np 16 --oversubscribe python3 run_detector.py --train_detector --exp test_detector`


## Gazebo / subt? ##
`roslaunch titan_rl train_titan_rl.launch agents:=16` </br>
`mpirun -np 16 --oversubscribe python3 titan_rl.py --exp vis_just_aux4 --just_aux` </br>
`roslaunch titan_rl rviz_debug.launch` </br>




## Pybullet / other ##

* RL for DTR. The algorithm used is Proximal Policy Optimisation (PPO), adapted from https://github.com/openai/baselines

### How do I get set up? ###

#### Dependencies: ####
- pybullet (for pybullet sim) </br>
- `pip3 install pybullet` </br>
- tensorflow==1.14  </br>
- `pip3 install tensorflow==1.14` </br>
- baselines (from https://github.com/openai/baselines follow install instructions) </br>
- Probably others.. TODO </br>

#### Launching: ####
- roslaunch titan_rl train_titan_rl.launch agents:=8
Which launches simulation_agent.launch and empty_world.launch (from titan_rl)
- worlds/empty.world is also from titan_rl/launch (this is so we can play with physics parameters easily)

#### To train: ####
`mpirun -np 16 python3 run.py --exp test --folder d` </br>
- will save tensorboard plots etc to /home/User/results/dtr/latest/d/test</br>

#### During training: ####
* Display while training (run at anytime, will show training in pybullet):</br>
 `python3 display.py --exp d/test`</br>
* Tensorboard to view learning curves:</br>
`tensorboard --logdir /home/User/results/rl_hex/latest/h` </br>

#### To test: ####
`python3 run.py --exp save_location --test_pol`</br>

#### Sim2real ideas: ####
- Learn an actuator model, need command and label (Learning Agile and Dynamic Motor Skills for Legged Robots - Hwangbo et al) </br>
- Dynamics and Domain randomisation  (Sim-to-Real Transfer of Robotic Control with Dynamics Randomization - Peng et al) </br>
- Latent encoding of different dynamics properties  (	Learning Agile Robotic Locomotion Skills by Imitating Animals - Peng et al) </br>
External forces, increasing magnitude. 
Noise to state input, noise on actions.
URDF parameters: mass, inertia, links, joint positions
Physics sim parameters: friction, damping, 
Const std for training
Training on real robot - SAC?

