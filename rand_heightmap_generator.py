'''
Generate positive (gaps 0.8-1.0m) examples in front of robot, 
and negative (gaps < 0.8), nothing, occusions of the door
'''
import warnings
warnings.filterwarnings('ignore')
import numpy as np
import cv2
from models import ppo_aux
import scripts.utils as U
import scripts.mpi_utils as MPI_U
import argparse
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
import tensorboardX
from scripts import logger
import random
from mpi4py import MPI
from pathlib import Path
home = str(Path.home())
import json
import git
import rospy
np.set_printoptions(precision=3, suppress=True)
import time 

def transform_rot3d(yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), 0],
            [np.sin(yaw), np.cos(yaw), 0],
            [0, 0, 1]])
    # return np.add(np.dot(rot_mat,points.T).T, pos).astype(np.int32)
    return np.add(np.dot(rot_mat,points.T).T, pos)

def transform_rot2d(yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw)],
            [np.sin(yaw), np.cos(yaw)]])
    return np.add(np.dot(rot_mat,points.T).T, pos).astype(np.int32)

class HeightMapGenerator():
    def __init__ (self, im_size=[80,80,1]):
        self.im_size = im_size


    def template(self, image, image_c, size, w, l, num, left):

        if num == 0:
            # -
            c1_1 = [image_c[1] + left*size/2, image_c[0]]
            c1_2 = [image_c[1] + left*(size/2 + l), image_c[0]]
            c1_3 = [image_c[1] + left*(size/2 + l), image_c[0] + w]
            c1_4 = [image_c[1] + left*size/2, image_c[0] + w]
            return np.array([c1_1, c1_2, c1_3, c1_4], np.int32)

        if num == 1:
            # |
            c1_1 = [image_c[1] + left*size/2, image_c[0]]
            c1_2 = [image_c[1] + left*(size/2 + w), image_c[0]]
            c1_3 = [image_c[1] + left*(size/2 + w), image_c[0] + l]
            c1_4 = [image_c[1] + left*size/2, image_c[0] + l]
            return np.array([c1_1, c1_2, c1_3, c1_4], np.int32)

        if num == 2:
            # L
            c1_1 = [image_c[1] + left*(size/2 + l), image_c[0]]
            c1_2 = [image_c[1] + left*(size/2 + l), image_c[0] + w]
            c1_3 = [image_c[1] + left*(size/2), image_c[0] + w]
            c1_4 = [image_c[1] + left*(size/2), 0]
            c1_5 = [image_c[1] + left*((size/2) + w), 0]
            c1_6 = [image_c[1] + left*((size/2) + w), image_c[0]]
            return np.array([c1_1, c1_2, c1_3, c1_4, c1_5, c1_6], np.int32)

        if num == 3:
            # T
            c1_1 = [image_c[1] + left*size/2, image_c[0]]
            c1_2 = [image_c[1] + left*(size/2 + l), image_c[0]]
            c1_3 = [image_c[1] + left*(size/2 + l), image_c[0] + w]
            c1_4 = [image_c[1] + left*(size/2 + w), image_c[0] + w]
            c1_5 = [image_c[1] + left*(size/2 + w), image_c[0] + w + image.shape[1]]
            c1_6 = [image_c[1] + left*(size/2), image_c[0] + w + image.shape[1]]
            return np.array([c1_1, c1_2, c1_3, c1_4, c1_5, c1_6], np.int32)

        if num == 4:
            c1_1 = [image_c[1] + left*size/2, image_c[0]]
            c1_2 = [image_c[1] + left*(size/2 + l), image_c[0]]
            c1_3 = [image_c[1] + left*(size/2 + l), image_c[0] + w]
            if left == 1:
                door_angle = np.random.uniform(-np.pi/2, np.pi/2)
            else:
                door_angle = np.random.uniform(np.pi/2, np.pi*3/2)
            x1, y1 = image_c[1] + left*(size/2 + w), image_c[0] + w
            x2, y2 = image_c[1] + left*(size/2), image_c[0] + w
            c1_4 = [x1, y1]            
            # c1_5 = [image.shape[1]*np.cos(door_angle) + x1, image.shape[1]*np.sin(door_angle) + y1]
            # c1_6 = [image.shape[1]*np.cos(door_angle) + x2, image.shape[1]*np.sin(door_angle) + y2]
            c1_5 = [image.shape[1]*np.cos(door_angle) + x1, image.shape[1]*np.sin(door_angle) + y1]
            c1_6 = [image.shape[1]*np.cos(door_angle) + x2, image.shape[1]*np.sin(door_angle) + y2]
            c1_7 = [x2, y2]

            return np.array([c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7], np.int32)

    def door_template(self, width=0.2, length=1.0, door_size=0.8, grid_size=0.1, width2=0.0, temp_type=[1,1], return_boxes=False, show=False, radius=1.5):
        
        w = int(width/grid_size)
        l = int(length/grid_size)
        size = int(door_size/grid_size)

        w2 = int(width2/grid_size)
        # passed door:
        image = np.zeros([w*2 + w2*2, l*2 + size, 1])
        image_c = [image.shape[0]/2, image.shape[1]/2]

        pts1 = self.template(image, image_c, size, w, l, num=temp_type[0], left=1)
        pts2 = self.template(image, image_c, size, w, l, num=temp_type[1], left=-1)
        
        pts1 = pts1.reshape((-1,1,2))
        cv2.fillPoly(image, [pts1], True, 1)

        pts2 = pts2.reshape((-1,1,2))
        cv2.fillPoly(image, [pts2], True, 1)
        
        # if show:       
        #     cv2.imshow('template', image)
        #     cv2.waitKey(0)

        if temp_type in [[2,2]]:
            # pt_before = [-0.5 - width2, 0.0]
            # pt_before = [-1.0 - width2, 0.0]
            pt_before = [-1.0 - width2, 0.0]
        else:
            # pt_before = [-0.5, 0.0]
            # pt_before = [-1.0, 0.0]
            pt_before = [-1.0, 0.0]

        if temp_type in [[1,1]]:
            # pt_after = [0.5 + length + width, 0.0]
            pt_after = [radius + length + width, 0.0]
        elif temp_type in [[3,3]]:
            # pt_after = [0.5 + width2 + width, 0.0]
            pt_after = [radius + width2 + width, 0.0]
        else:
            # pt_after = [0.5 + width*2, 0.0]
            pt_after = [radius + width*2, 0.0]

        return image, pt_before, pt_after


    def insert_template(self, img, template, robot_frame_x, robot_frame_y, yaw, pt_before, pt_after):
        x, y = self.robot_to_image([robot_frame_x, robot_frame_y], img.shape)
        temp_x, temp_y = int(template.shape[0]/2), int(template.shape[1]/2)
        ij = np.array([(i,j,template[i+temp_x,  j+temp_y]) for i in range(-temp_x, temp_x) for j in range(-temp_y, temp_y)], dtype='object')

        pts = transform_rot3d(yaw, [x, y, 0], ij)
        for x,y,z in pts:
            if x > 0 and x < img.shape[0] and y > 0 and y < img.shape[1]:
                img[int(x),int(y)] = z

        pt_before = self.robot_to_image(pt_before, img.shape)
        pt_after = self.robot_to_image(pt_after, img.shape)        
        return pt_before, pt_after

    def robot_to_image(self, point, im_size, grid_size=0.1):
        x = int(point[0]/grid_size + self.im_size[0]/2) 
        y = int(point[1]/grid_size + self.im_size[1]/2)  
        return x, y

    def rand_template(self, img, width, length, width2, temp_type, robot_frame_x, robot_frame_y, yaw, show=False, grid_size=0.1):
    # def door_template(self, width=0.2, length=1.0, door_size=0.8, grid_size=0.1, width2=0.0, temp_type=[1,1], return_boxes=False, show=False):
        
        w = int(width/grid_size)
        l = int(length/grid_size)
        # size = int(door_size/grid_size)
        size = 0

        w2 = int(width2/grid_size)
        image = np.zeros([w*2 + w2*2, l*2 + size, 1])
        image_c = [image.shape[0]/2, image.shape[1]/2]

        pts1 = self.template(image, image_c, size, w, l, num=temp_type, left=1)
        
        pts1 = pts1.reshape((-1,1,2))
        cv2.fillPoly(image, [pts1], True, 1)
        
        x, y = self.robot_to_image([robot_frame_x, robot_frame_y], img.shape)
        temp_x, temp_y = int(image.shape[0]/2), int(image.shape[1]/2)
        ij = np.array([(i,j,image[i+temp_x,  j+temp_y]) for i in range(-temp_x, temp_x) for j in range(-temp_y, temp_y)], dtype='object')

        pts = transform_rot3d(yaw, [x, y, 0], ij)
        for x,y,z in pts:
            if x > 0 and x < img.shape[0] and y > 0 and y < img.shape[1]:
                img[int(x),int(y)] = z

    def get_batch(self, batch_size=256, show=False):

        # radius = 1.5
        radius = 2.0
        radius_sqrd = radius * radius
        grid_size = 0.1
        imgs = []
        inputs = []
        labels = []
        pt_after = None
        pt_before = None
        for _ in range(batch_size):
            # rand_type = np.random.choice(["nothing", "negative", "positive"], p=[0.05, 0.15, 0.8])
            rand_type = np.random.choice(["nothing", "negative", "positive"], p=[0.15, 0.4, 0.45])
            # rand_type = "negative"
            if rand_type == "negative":
                # x, y = np.random.uniform(-3.5, -0.5), np.random.uniform(-3.5, 3.5)
                x, y = np.random.uniform(-3.5, 3.5), np.random.uniform(-3.5, 3.5)
                yaw = np.random.uniform(-1.57, 1.57)
                # door_size = np.random.uniform(0.4, 0.8)
                door_size = np.random.uniform(0.4, 1.5)
            elif rand_type == "positive":
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd or (pt_after[0] - robot_x)**2 + (pt_after[1] - robot_y)**2 < 0.5**2:

                # x, y = np.random.uniform(0, 3.5), np.random.uniform(-3.5, 3.5)
                # x, y = np.random.uniform(-2.0, 2.0), np.random.uniform(-3.5, 3.5)
                # x, y = np.random.uniform(-3.5, 3.5), np.random.uniform(-3.5, 3.5)
                # x, y = np.random.uniform(-1.5, radius*3), np.random.uniform(-radius*3, radius*3)
                # x, y = np.random.uniform(-0.5, radius*2), np.random.uniform(-radius, radius)
                # x, y = np.random.uniform(-radius, radius), np.random.uniform(-radius, radius)
                # x, y = (radius/2)*np.cos(np.random.uniform(0, 2*np.pi)), (radius/2)*np.sin(np.random.uniform(0, 2*np.pi))

                # t = 
                # r = radius * np.sqrt(np.random.uniform(0.0, 1.0, num))
                # r = radius * np.sqrt(np.random.uniform(0.0, 1.0, num))
                
                x = np.sqrt(np.random.uniform(0, radius)) * np.cos(np.random.uniform(0.0, 2.0*np.pi))
                y = np.sqrt(np.random.uniform(0, radius)) * np.sin(np.random.uniform(0.0, 2.0*np.pi))

                # if x < 0:
                #     y = np.random.uniform(-0.5, 0.5)
                # yaw = np.clip(np.arctan2(y,x) + np.random.uniform(-0.707, 0.707), -1.5, 1.5)
                # yaw = np.arctan2(y,x) + np.random.uniform(-0.707, 0.707)
                # yaw = np.arctan2(y,x) + np.random.uniform(-0.707, 0.707)
                yaw = np.random.uniform(-1.57, 1.57)
                # Target is 0.8 m with 0.2 tolerance
                # door_size = np.random.uniform(0.6, 1.0)
                door_size = np.random.uniform(0.7, 1.0)
            else:
                x, y = np.random.uniform(-3.5, 3.5), np.random.uniform(-3.5, 3.5)
                yaw = np.random.uniform(-1.57, 1.57)                
                door_size = np.random.uniform(0.4, 1.5)

            width = np.random.uniform(0.1,0.4)
            # width = 0.4
            # length = np.random.uniform(0.5,3)
            length = np.random.uniform(4.0,8.0)
            # width2 = np.random.uniform(0.5,3.0)
            width2 = np.random.uniform(0.5,1.5)

            img = np.zeros(self.im_size, dtype=np.float32)
            # img = np.random.randint(2, size=self.im_size).astype(dtype=np.float32)

            if rand_type != "nothing":
                # door_type = [np.random.randint(0,4), np.random.randint(0,4)]
                # door_type = [4,0]
                # choices = [[4,0],[0,4]]
                # door_type = choices[np.random.randint(0,2)]
                # door_type = [4,0]
                door_type = [4,4]
                # door_type = [0,4]

                # door_type = [0,0]
                template, pt_before, pt_after = self.door_template(width=width, length=length, door_size=door_size, width2=width2, temp_type=door_type, show=show, radius=radius)
                pt_before = transform_rot3d(yaw, [x, y, 0], np.array([pt_before[0], pt_before[1], 0]))
                pt_after = transform_rot3d(yaw, [x, y, 0], np.array([pt_after[0], pt_after[1], 0]))
                img_pt_before, img_pt_after = self.insert_template(img, template, robot_frame_x=x, robot_frame_y=y, yaw=yaw, pt_before=pt_before, pt_after=pt_after)
            
            # Add random block:
            # if rand_type == "positive":
            # rand_blocks = np.random.randint(0,3)
            # rand_blocks = 3
            # for b in range(rand_blocks): 
            if rand_type != "nothing":    
                if x < 0:               
                    dist_x = x + np.random.uniform(3.5,4.5)*np.cos(yaw)
                    # dist_x = np.random.uniform(0.5,3.5)
                else:
                    dist_x = x - np.random.uniform(3.5,4.5)*np.cos(yaw)
                    # dist_x = np.random.uniform(-3.5,-0.5)
                if y < 0:               
                    # dist_y = np.random.uniform(0.5,3.5)
                    dist_y = y + np.random.uniform(3.5,4.5)*np.sin(yaw)
                else:
                    dist_y = y - np.random.uniform(3.5,4.5)*np.sin(yaw)
                    # dist_y = np.random.uniform(-3.5,-0.5)
                # dist_yaw = np.random.uniform(-1.0, 1.0)
                dist_yaw = yaw + np.random.uniform(-0.5, 0.5)                

                width = np.random.uniform(0.2,0.3)
                # length = np.random.uniform(0.5,2)
                length = np.random.uniform(5,10)
                width2 = np.random.uniform(0.5,2)
                # self.rand_template(img=img, width=width, length=length, width2=width2, temp_type=np.random.randint(0,4), robot_frame_x=dist_x, robot_frame_y=dist_y, yaw=dist_yaw, show=show)
                self.rand_template(img=img, width=width, length=length, width2=width2, temp_type=0, robot_frame_x=dist_x, robot_frame_y=dist_y, yaw=dist_yaw, show=show)


            if rand_type == "positive":
                # input_x1, input_y1 = pt_before[0] + np.random.uniform(-radius, radius), pt_before[1] + np.random.uniform(-radius, radius)
                # # input_x1, input_y1 = [0, 0]
                # input_x2, input_y2 = pt_after[0] + np.random.uniform(-radius, radius), pt_after[1] + np.random.uniform(-radius, radius)
                input_x2 = pt_after[0] + np.sqrt(np.random.uniform(0, radius)) * np.cos(np.random.uniform(0.0, 2.0*np.pi))
                input_y2 = pt_after[1] + np.sqrt(np.random.uniform(0, radius)) * np.sin(np.random.uniform(0.0, 2.0*np.pi))

                # input_x2, input_y2 = pt_after[0] + radius*np.cos(np.random.uniform(0, 2*np.pi)), pt_after[1] + radius*np.sin(np.random.uniform(0, 2*np.pi))
                # input_x2, input_y2 = pt_after[0] , pt_after[1] 
            else:
                # input_x1, input_y1 = np.random.uniform(-10, 10), np.random.uniform(-10, 10)
                input_x2, input_y2 = np.random.uniform(-10, 10), np.random.uniform(-10, 10)

            if rand_type != "nothing" and door_size > 0.6 and door_size <= 1.0:
                # Make sure point after door is at least in front of the robot.
                pt_before_angle = np.arctan2(pt_before[1],pt_before[0])
                pt_before_dist = np.sqrt( pt_before[1]*pt_before[1] + pt_before[0]*pt_before[0] )
                pt_after_angle = np.arctan2(pt_after[1],pt_after[0])
                pt_after_dist = np.sqrt( pt_after[1]*pt_after[1] + pt_after[0]*pt_after[0] )
                # Not just 'is there a door', does 'two provided points pass through a door'.
                # 10 radius = 10*grid_size = 1m

                # if (pt_before[0] - input_x1)**2 + (pt_before[1] - input_y1)**2 < radius_sqrd and (pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2 < radius_sqrd:
                #     label = True
                # if pt_after_angle < 1.0 and pt_after_angle > -1.0 and (pt_after_dist < 3.5 or pt_before_dist < 3.5): 
                robot_x, robot_y = 0,0
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd or (pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2 < radius_sqrd:
                # if robot in before circle, or robot close to center of after circle (0.5m to center of after circle).
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd or (pt_after[0] - robot_x)**2 + (pt_after[1] - robot_y)**2 < 0.5**2:
                # if robot in before circle, or robot in first half of after circle
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd or ((pt_after[0] - robot_x)**2 + (pt_after[1] - robot_y)**2 < radius_sqrd and x > -1.5):
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd and pt_after[0] > 0 and (pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2 < radius_sqrd and pt_after[0] > 0 :
                # if (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 < radius_sqrd and pt_after[0] > 0 and (pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2 < radius_sqrd:
                if ((pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2) <= radius_sqrd and ((pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2) <= radius_sqrd:
                # if robot in before circle, or robot in after circle.
                    labels.append([1, door_size, pt_before_angle, pt_before_dist, pt_after_angle, pt_after_dist])
                else:
                    labels.append([0, 0, 0, 0, 0, 0])
            else:
                labels.append([0, 0, 0, 0, 0, 0])
            
            # print("door size", door_size > 0.6 and door_size <= 1.0)
            # print("condition", (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 <= radius_sqrd and (pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2 <= radius_sqrd)
            # print("parts", (pt_before[0] - robot_x)**2 + (pt_before[1] - robot_y)**2 <= radius_sqrd ,  ((pt_after[0] - input_x2)**2 + (pt_after[1] - input_y2)**2) <= radius_sqrd)

            # inputs.append([input_x1, input_y1, input_x2, input_y2])
            inputs.append([0, 0, input_x2, input_y2])
            imgs.append(img)

            if show:
                col_img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)

                cv2.circle(col_img, (40, 40), 2, (0,255,0))

                if pt_after is not None:
                    input_image_x2, input_image_y2 = self.robot_to_image([input_x2, input_y2], img.shape)
                    # rect = [[input_image_y2 - 10, input_image_x2 - 10], [input_image_y2 + 10, input_image_x2 - 10], [input_image_y2 + 10, input_image_x2 + 10], [input_image_y2 - 10, input_image_x2 + 10]]
                    if labels[-1][0]:
                        cv2.circle(col_img, (input_image_y2, input_image_x2), 2, (0,255,0))
                        # cv2.rectangle(col_img, rect, 1, (0,0,255))
                        # cv2.polylines(col_img, np.int32([rect]), True, (0,0,255), 1)
                        cv2.circle(col_img, (img_pt_after[1], img_pt_after[0]), int(radius/grid_size), (0,255,0))
                    else:
                        cv2.circle(col_img, (input_image_y2, input_image_x2), 2, (0,0,255))
                        # cv2.polylines(col_img, np.int32([rect]), True, (0,255,0), 1)
                    # cv2.circle(col_img, (img_pt_after[1], img_pt_after[0]), int(radius/grid_size), (0,255,0))
                        cv2.circle(col_img, (img_pt_after[1], img_pt_after[0]), int(radius/grid_size), (0,0,255))

                # image_x1, image_y1 = self.robot_to_image([x1, y1], img.shape)
                # cv2.circle(col_img, (image_y1, image_x1), 2, (0,255,0))
                if pt_before is not None:
                    # input_image_x1, input_image_y1 = self.robot_to_image([input_x1, input_y1], img.shape)
                    # rect = [[input_image_y1 - 10, input_image_x1 - 10], [input_image_y1 + 10, input_image_x1 - 10], [input_image_y1 + 10, input_image_x1 + 10], [input_image_y1 - 10, input_image_x1 + 10]]
                    if labels[-1][0]:
                        # cv2.circle(col_img, (input_image_y1, input_image_x1), 1, (0,255,0))
                        cv2.circle(col_img, (img_pt_before[1], img_pt_before[0]), int(radius/grid_size), (0,255,0))
                        # cv2.rectangle(col_img, rect, 1, (0,0,255))
                        # cv2.polylines(col_img, np.int32([rect]), True, (0,0,255), 1)
                        # cv2.circle(col_img, (img_pt_before[1], img_pt_before[0]), 1, (0,255,0))
                    else:
                        # cv2.circle(col_img, (input_image_y1, input_image_x1), 1, (0,0,255))
                        # cv2.polylines(col_img, np.int32([rect]), True, (0,255,0), 1)
                        cv2.circle(col_img, (img_pt_before[1], img_pt_before[0]), int(radius/grid_size), (0,0,255))

                cv2.imshow('frame', col_img)
                cv2.waitKey(0)

        return np.array(imgs), np.array(labels), np.array(inputs)

def run(args):
    im_size = [80,80,1]
    hm_gen = HeightMapGenerator(im_size)

    imgs, labels, ob = hm_gen.get_batch(10, show=True)


if __name__=="__main__":

    import defaults
    from dotmap import DotMap
    args1, unknown1 = defaults.get_defaults() 
    parser = argparse.ArgumentParser()
    # Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
    parser.add_argument('--folder', default='t1')
    parser.add_argument('--train_aux', default=True, action='store_false')
    parser.add_argument('--aux_size', default=5, type=int)
    parser.add_argument('--aux_lr', default=1e-3, type=float)
    args2, unknown2 = parser.parse_known_args()
    args2 = vars(args2)
    # Replace any arguments from defaults with run specific defaults
    for key in args2:
        args1[key] = args2[key]
    # Look for any changes to defaults (unknowns) and replace values in args1
    for n, unknown in enumerate(unknown2):
        if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
            arg_type = type(args1[unknown[2:]])
            args1[unknown[2:]] = arg_type(unknown2[n+1])
    args = DotMap(args1)
    # Check for dodgy arguments
    unknowns = []
    for unknown in unknown1 + unknown2:
        if "--" in unknown and unknown[2:] not in args:
            unknowns.append(unknown)
    if len(unknowns) > 0:
        print("Dodgy argument")
        print(unknowns)
        exit()
    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
    run(args)

    