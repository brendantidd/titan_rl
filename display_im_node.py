#! /usr/bin/env python3
# '''
# Subscribe to images from each worker
# Display world map + worker box + image from each worker
# '''
import sys
import rospy
import cv2
from std_msgs.msg import String, ColorRGBA
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import ros_numpy
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
from visualization_msgs.msg import Marker, MarkerArray
import numpy as np
from nav_msgs.msg import OccupancyGrid, MapMetaData
from geometry_msgs.msg import Pose, Point, Vector3, Quaternion
from scipy.spatial.transform import Rotation

class DisplayIM:

    def __init__(self, im_size=[60,60,1], num_workers=8, aux_size=6):
        self.im_size = im_size
        self.grid_size = 0.1
        self.aux_size = aux_size
        # self.image_pub = rospy.Publisher("world_image", Image, queue_size=1)
        # self.bridge = CvBridge()

        # rospy.Subscriber('/' + self.robot_name + '/heightmap_to_costmap/occupancy_grid', OccupancyGrid, self.hmCallback)
        self.image_pub = rospy.Publisher('/world_occupancy_grid', OccupancyGrid, queue_size=1)
        
        self.door_markers_pub = rospy.Publisher('door_markers', MarkerArray, queue_size=1)        
        self.robot_markers_pub = rospy.Publisher('robot_markers', MarkerArray, queue_size=1)        
        self.reset_markers()

        # self.image_sub = rospy.Subscriber("image_topic",Image,self.callback)
        self.image_sub = rospy.Subscriber("/r1/hm", numpy_msg(Floats), self.callback1)
        self.image_sub = rospy.Subscriber("/r2/hm", numpy_msg(Floats), self.callback2)
        self.image_sub = rospy.Subscriber("/r3/hm", numpy_msg(Floats), self.callback3)
        self.image_sub = rospy.Subscriber("/r4/hm", numpy_msg(Floats), self.callback4)
        self.image_sub = rospy.Subscriber("/r5/hm", numpy_msg(Floats), self.callback5)
        self.image_sub = rospy.Subscriber("/r6/hm", numpy_msg(Floats), self.callback6)
        self.image_sub = rospy.Subscriber("/r7/hm", numpy_msg(Floats), self.callback7)
        self.image_sub = rospy.Subscriber("/r8/hm", numpy_msg(Floats), self.callback8)
        self.image_sub = rospy.Subscriber("/r9/hm", numpy_msg(Floats), self.callback9)
        self.image_sub = rospy.Subscriber("/r10/hm", numpy_msg(Floats), self.callback10)
        self.image_sub = rospy.Subscriber("/r11/hm", numpy_msg(Floats), self.callback11)
        self.image_sub = rospy.Subscriber("/r12/hm", numpy_msg(Floats), self.callback12)
        self.image_sub = rospy.Subscriber("/r13/hm", numpy_msg(Floats), self.callback13)
        self.image_sub = rospy.Subscriber("/r14/hm", numpy_msg(Floats), self.callback14)
        self.image_sub = rospy.Subscriber("/r15/hm", numpy_msg(Floats), self.callback15)
        self.image_sub = rospy.Subscriber("/r16/hm", numpy_msg(Floats), self.callback16)
        self.image_sub = rospy.Subscriber("/r1/goal", numpy_msg(Floats), self.callbackGoal1)
        self.image_sub = rospy.Subscriber("/r2/goal", numpy_msg(Floats), self.callbackGoal2)
        self.image_sub = rospy.Subscriber("/r3/goal", numpy_msg(Floats), self.callbackGoal3)
        self.image_sub = rospy.Subscriber("/r4/goal", numpy_msg(Floats), self.callbackGoal4)
        self.image_sub = rospy.Subscriber("/r5/goal", numpy_msg(Floats), self.callbackGoal5)
        self.image_sub = rospy.Subscriber("/r6/goal", numpy_msg(Floats), self.callbackGoal6)
        self.image_sub = rospy.Subscriber("/r7/goal", numpy_msg(Floats), self.callbackGoal7)
        self.image_sub = rospy.Subscriber("/r8/goal", numpy_msg(Floats), self.callbackGoal8)
        self.image_sub = rospy.Subscriber("/r9/goal", numpy_msg(Floats), self.callbackGoal9)
        self.image_sub = rospy.Subscriber("/r10/goal", numpy_msg(Floats), self.callbackGoal10)
        self.image_sub = rospy.Subscriber("/r11/goal", numpy_msg(Floats), self.callbackGoal11)
        self.image_sub = rospy.Subscriber("/r12/goal", numpy_msg(Floats), self.callbackGoal12)
        self.image_sub = rospy.Subscriber("/r13/goal", numpy_msg(Floats), self.callbackGoal13)
        self.image_sub = rospy.Subscriber("/r14/goal", numpy_msg(Floats), self.callbackGoal14)
        self.image_sub = rospy.Subscriber("/r15/goal", numpy_msg(Floats), self.callbackGoal15)
        self.image_sub = rospy.Subscriber("/r16/goal", numpy_msg(Floats), self.callbackGoal16)
        
        self.world_sub = rospy.Subscriber("/world", numpy_msg(Floats), self.callbackWorld)
        # self.pos = {i:{'x':0,'y':0,'yaw':0} for i in range(1, num_workers+1)}
        self.pos = {i:None for i in range(1, num_workers+1)}
        self.aux = {i:[] for i in range(1, num_workers+1)}
        self.est_aux = {i:[] for i in range(1, num_workers+1)}
        # self.world_hm = {i:np.zeros(self.im_size, dtype=np.float32) for i in range(0, num_workers+1)}
        self.world_hm = None
        self.hms = {i:np.zeros(self.im_size, dtype=np.float32) for i in range(1, num_workers+1)}
        self.has_hm = {i:False for i in range(0, num_workers)}
        self.world_offset = [0,0,0]
        self.goal_pos = {i:None for i in range(1, num_workers+1)}
        self.local_goal_pos = {i:None for i in range(1, num_workers+1)}



    def reset_markers(self):
        self.doors = MarkerArray()
        self.robots = MarkerArray()

    def make_grid_from_numpy(self, data):
        grid = OccupancyGrid()
        grid.data = list(data.T.astype(np.int8).ravel() - 1)
        grid.info = MapMetaData()
        grid.info.height = self.world_hm.shape[1]
        grid.info.width = self.world_hm.shape[0]
        grid.info.resolution = 0.1
        pose = Pose()
        pose.position.x = -(self.world_width/2)*self.grid_size
        pose.position.y = -(self.world_height/2)*self.grid_size
        pose.position.z = 0
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1
        grid.info.origin = pose
        return grid

    def pub_world(self, info=None):
        # print(self.world_hm.T.shape)
        self.image_pub.publish(self.make_grid_from_numpy(self.world_hm))

    def pub_markers(self):
        self.door_markers_pub.publish(self.doors)
        self.robot_markers_pub.publish(self.robots)

    def make_arrow_points_marker(self, tail, tip, num, col):
        # make a visualization marker array for the occupancy grid
        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = '/map'
        m.header.stamp = rospy.Time.now()
        # m.ns = 'points_arrows'
        m.ns = num
        # m.id = num
        m.type = Marker.ARROW
        m.pose.orientation.y = 0
        m.pose.orientation.w = 1
        # m.scale = scale
        m.scale = Vector3(0.2,0.5,0.5)
        m.color = col
        # m.color.r = 0.2
        # m.color.g = 0.5
        # m.color.b = 1.0
        # m.color.a = 1.0
        m.points = [ tail, tip ]
        return m

    # def make_marker(marker_type, scale, r, g, b, a):
    #     # make a visualization marker array for the occupancy grid
    #     m = Marker()
    #     m.action = Marker.ADD
    #     m.header.frame_id = '/base_link'
    #     m.header.stamp = rospy.Time.now()
    #     m.ns = 'marker_test_%d' % marker_type
    #     m.id = 0
    #     m.type = marker_type
    #     m.pose.orientation.y = 0
    #     m.pose.orientation.w = 1
    #     m.scale = scale
    #     m.color.r = 1.0
    #     m.color.g = 0.5
    #     m.color.b = 0.2
    #     m.color.a = 0.3
    #     m.color.r = r
    #     m.color.g = g
    #     m.color.b = b
    #     m.color.a = a
    #     return m

    def make_line(self, tail, tip, num, col):
        # make a visualization marker array for the occupancy grid
        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = '/map'
        m.header.stamp = rospy.Time.now()
        m.ns = num
        # m.id = num 
        m.type = Marker.LINE_LIST
        # m.pose.position.x = pos[0]
        # m.pose.position.y = pos[1]
        # m.pose.position.z = pos[2]
        # m.pose.orientation.x = orn[0]
        # m.pose.orientation.y = orn[1]
        # m.pose.orientation.z = orn[2]
        m.pose.orientation.w = 1
        m.scale = Vector3(0.1, 1.0, 1.0)
        # m.color.a = 1.0
        # m.color.r = 1.
        # color = ColorRGBA(1,0,0,1)
        color = col

        m.colors = [color, color]
        # m.colors.push_back(color);
        m.points = [ tail, tip ]
        return m

    # def make_marker(marker_type, scale, r, g, b, a):
    def make_marker(self, pos, orn, num, size=[1.0,1.0,1.0], col= [1.0,0.0,0.0,1.0],  marker_type='mesh'):
        # make a visualization marker array for the occupancy grid
        m = Marker()
        m.action = Marker.ADD
        m.header.frame_id = '/map'
        m.header.stamp = rospy.Time.now()
        m.ns = num
        # m.id = num 
        if marker_type == 'mesh':
            m.type = Marker.MESH_RESOURCE
            m.mesh_resource = "package://titan_rl/assets/Fixed_Titan.stl"
        elif marker_type == 'sphere':
            m.type = Marker.SPHERE
        m.pose.position.x = pos[0]
        m.pose.position.y = pos[1]
        m.pose.position.z = pos[2]
        m.pose.orientation.x = orn[0]
        m.pose.orientation.y = orn[1]
        m.pose.orientation.z = orn[2]
        m.pose.orientation.w = orn[3]
        m.scale = Vector3(size[0], size[1], size[2])
        m.color.a = col[3]
        m.color.r = col[0]
        m.color.g = col[1]
        m.color.b = col[2]
        return m

    def add_markers(self, num):
        robot_x, robot_y, robot_yaw = self.pos[num]['x'], self.pos[num]['y'], self.pos[num]['yaw']
        pos = [robot_x, robot_y, 0.0]

        rot = Rotation.from_euler('xyz', [0, 0, robot_yaw], degrees=False)        
        orn = rot.as_quat() 
        marker_num = "marker_" + str(num)
        self.robots.markers.append(self.make_marker(pos, orn, marker_num))

        # self.aux = [1.0, door_size, self.goal_angle1, self.goal_dist1, self.goal_angle2, self.goal_dist2]
        if self.aux[num][0] > 0:
            arrow_num = "aux_arrow_" + str(num)
            line_num = "aux_line_" + str(num)
            size, angle1, dist1, angle2, dist2 = self.aux[num][1], self.aux[num][2], self.aux[num][3], self.aux[num][4],  self.aux[num][5]
            # If door_x, door_y provided, uncomment this: 
            # door_x, door_y, target_angle, size = self.aux[num][1], self.aux[num][2], self.aux[num][3], self.aux[num][4]
            # And comment out this:
            door_x1, door_y1 = dist1*np.cos(angle1+robot_yaw) + robot_x, dist1*np.sin(angle1+robot_yaw) + robot_y  
            door_x2, door_y2 = dist2*np.cos(angle2+robot_yaw) + robot_x, dist2*np.sin(angle2+robot_yaw) + robot_y  
            
            # door_x2, door_y2 = size*np.cos(target_angle) + door_x, size*np.sin(target_angle) + door_y

            tail = Point(door_x1, door_y1, 0)
            tip = Point(door_x2, door_y2, 0)
            col = ColorRGBA(1,0,0,1)
            
            self.doors.markers.append(self.make_arrow_points_marker(tail, tip, arrow_num, col))

            target_angle = np.arctan2(door_y2 - door_y1, door_x2 - door_x1)
            x1, y1 = door_x1 + (size/2)*np.cos(target_angle - np.pi/2), door_y1 + (size/2)*np.sin(target_angle-np.pi/2)
            x2, y2 = door_x1 - (size/2)*np.cos(target_angle - np.pi/2), door_y1 - (size/2)*np.sin(target_angle-np.pi/2)

            tail = Point(x1, y1, 0)
            tip = Point(x2,y2,0)
            self.doors.markers.append(self.make_line(tail, tip, line_num, col))

        else:
            arrow_num = "aux_arrow_" + str(num)
            line_num = "aux_line_" + str(num)
            tail = Point(0, 0, 0)
            tip = Point(0, 0, 0)
            col = ColorRGBA(1,0,0,1)
            self.doors.markers.append(self.make_arrow_points_marker(tail, tip, arrow_num, col))
            self.doors.markers.append(self.make_line(tail, tip, line_num, col))

        # print(self.est_aux)
        if self.est_aux[num][0] > 0:
            est_arrow_num = "est_arrow_" + str(num)
            est_line_num = "est_line_" + str(num)
            size, angle1, dist1, angle2, dist2 = self.est_aux[num][1], self.est_aux[num][2], self.est_aux[num][3], self.est_aux[num][4],  self.est_aux[num][5]
            # If door_x, door_y provided, uncomment this: 
            # door_x, door_y, target_angle, size = self.aux[num][1], self.aux[num][2], self.aux[num][3], self.aux[num][4]
            # And comment out this:
            door_x1, door_y1 = dist1*np.cos(angle1+robot_yaw) + robot_x, dist1*np.sin(angle1+robot_yaw) + robot_y  
            door_x2, door_y2 = dist2*np.cos(angle2+robot_yaw) + robot_x, dist2*np.sin(angle2+robot_yaw) + robot_y  
            
            # door_x2, door_y2 = size*np.cos(target_angle) + door_x, size*np.sin(target_angle) + door_y

            tail = Point(door_x1, door_y1, 0)
            tip = Point(door_x2, door_y2, 0)

            
            col = ColorRGBA(0,1,0,1)
            self.doors.markers.append(self.make_arrow_points_marker(tail, tip, est_arrow_num, col))

            target_angle = np.arctan2(door_y2 - door_y1, door_x2 - door_x1)
            x1, y1 = door_x1 + (size/2)*np.cos(target_angle - np.pi/2), door_y1 + (size/2)*np.sin(target_angle-np.pi/2)
            x2, y2 = door_x1 - (size/2)*np.cos(target_angle - np.pi/2), door_y1 - (size/2)*np.sin(target_angle-np.pi/2)

            tail = Point(x1, y1, 0)
            tip = Point(x2,y2,0)
            self.doors.markers.append(self.make_line(tail, tip, est_line_num, col))

        else:
            est_arrow_num = "est_arrow_" + str(num)
            est_line_num = "est_line_" + str(num)
            col = ColorRGBA(0,1,0,1)
            tail = Point(0, 0, 0)
            tip = Point(0, 0, 0)
            self.doors.markers.append(self.make_arrow_points_marker(tail, tip, est_arrow_num, col))
            self.doors.markers.append(self.make_line(tail, tip, est_line_num, col))



    def set_data(self, data, num):
        self.pos[num] = {}
        self.pos[num]['x'] = int(data.data[0]) 
        self.pos[num]['y'] = int(data.data[1]) 
        self.pos[num]['yaw'] = data.data[2]
        

        # self.aux[num] = data.data[3:9]
        # self.est_aux[num] = data.data[9:14]
        # self.hms[num] = data.data[14:].reshape(self.im_size)
        # print(self.aux_size)
        self.aux[num] = data.data[3:(self.aux_size+3)]
        self.est_aux[num] = data.data[(self.aux_size+3):(self.aux_size*2+3)]
        self.hms[num] = data.data[(self.aux_size*2+3):].reshape(self.im_size)
        self.has_hm[num] = True

    def set_goal(self, data, num):
        # print(data)
        msg = data.data
        print(msg)
        pos = [msg[0], msg[1], 0]
        orn = [0,0,0,1]
        size = [1.0,1.0,0.1]
        num = "goal1_" + str(num)
        # print(num)
        col = [0.2, 1.0, 0.2, 1.0]
        self.robots.markers.append(self.make_marker(pos, orn, num, size, col, marker_type='sphere'))
        pos = [msg[2], msg[3], 0]
        col = [1.0, 0.2, 0.2, 1.0]
        num = "goal2_" + str(num)
        self.robots.markers.append(self.make_marker(pos, orn, num, size, col, marker_type='sphere'))


        # # print(num, np.array(data).shape)
        # self.goal_pos[num] = {}
        # self.goal_pos[num]['x'] = int(data.data[0]) 
        # self.goal_pos[num]['y'] = int(data.data[1]) 
        
        # self.local_goal_pos[num] = {}
        # self.local_goal_pos[num]['x'] = int(data.data[0]) 
        # self.local_goal_pos[num]['y'] = int(data.data[1]) 

    def callbackWorld(self, data):
        world_data = data.data
        self.im_size = [int(data.data[0]), int(data.data[1]), int(data.data[2])]
        self.world_offset = [data.data[3], data.data[4], data.data[5]]
        self.world_width = world_data[6]
        self.world_height = world_data[7]
        # self.world_hm = np.flip(world_data[8:].reshape([int(self.world_width), int(self.world_height)]), axis=1)
        # self.world_hm
        # self.world_hm = world_data[8:].reshape([int(self.world_width), int(self.world_height)]).T
        self.world_hm = world_data[8:].reshape([int(self.world_width), int(self.world_height)])

    def callback1(self, data):
        self.set_data(data, 1)

    def callback2(self, data):
        self.set_data(data, 2)

    def callback3(self, data):
        self.set_data(data, 3)

    def callback4(self, data):
        self.set_data(data, 4)
    
    def callback5(self, data):
        self.set_data(data, 5)
    
    def callback6(self, data):
        self.set_data(data, 6)
    
    def callback7(self, data):
        self.set_data(data, 7)
    
    def callback8(self, data):
        self.set_data(data, 8)

    def callback9(self, data):
        self.set_data(data, 9)

    def callback10(self, data):
        self.set_data(data, 10)

    def callback11(self, data):
        self.set_data(data, 11)

    def callback12(self, data):
        self.set_data(data, 12)

    def callback13(self, data):
        self.set_data(data, 13)

    def callback14(self, data):
        self.set_data(data, 14)

    def callback15(self, data):
        self.set_data(data, 15)

    def callback16(self, data):
        self.set_data(data, 16)

    def callbackGoal1(self, data):
        self.set_goal(data, 1)

    def callbackGoal2(self, data):
        self.set_goal(data, 2)

    def callbackGoal3(self, data):
        self.set_goal(data, 3)

    def callbackGoal4(self, data):
        self.set_goal(data, 4)
    
    def callbackGoal5(self, data):
        self.set_goal(data, 5)
    
    def callbackGoal6(self, data):
        self.set_goal(data, 6)
    
    def callbackGoal7(self, data):
        self.set_goal(data, 7)
    
    def callbackGoal8(self, data):
        self.set_goal(data, 8)

    def callbackGoal9(self, data):
        self.set_goal(data, 9)

    def callbackGoal10(self, data):
        self.set_goal(data, 10)

    def callbackGoal11(self, data):
        self.set_goal(data, 11)

    def callbackGoal12(self, data):
        self.set_goal(data, 12)

    def callbackGoal13(self, data):
        self.set_goal(data, 13)

    def callbackGoal14(self, data):
        self.set_goal(data, 14)

    def callbackGoal15(self, data):
        self.set_goal(data, 15)

    def callbackGoal16(self, data):
        self.set_goal(data, 16)

def transform_rot3d(yaw, pos, points):
    rot_mat = np.array(
            [[np.cos(yaw), -np.sin(yaw), 0],
            [np.sin(yaw), np.cos(yaw), 0],
            [0, 0, 1]])
    # return np.add(np.dot(rot_mat,points.T).T, pos).astype(np.int32)
    return np.add(np.dot(rot_mat,points.T).T, pos)


def run():
    num_workers = 16
    im_size = 80
    display = DisplayIM(im_size=[im_size, im_size, 1], num_workers=num_workers)
    rospy.init_node('display_im', anonymous=True)
    rate = rospy.Rate(10)

    # subt_hm = True
    # im_size = 60
    # x_min = int(-display.im_size[0]/2)
    # x_max = int(+display.im_size[0]/2)
    # y_min = int(-display.im_size[1]/2)
    # y_max = int(+display.im_size[1]/2)
    # x1, y1, x2, y2, x3, y3, x4, y4 = x_max, y_max, x_min, y_max, x_min, y_min, x_max, y_min
    # pts = np.array([[x1,y1,0],[x2,y2,0],[x3,y3,0],[x4,y4,0]])
    while not rospy.is_shutdown():
        rate.sleep()
        if display.world_hm is not None:
            display.pub_world()
            try:
                
                x_min = int(-display.im_size[0]/2)
                x_max = int(+display.im_size[0]/2)
                y_min = int(-display.im_size[1]/2)
                y_max = int(+display.im_size[1]/2)
                x1, y1, x2, y2, x3, y3, x4, y4 = x_max, y_max, x_min, y_max, x_min, y_min, x_max, y_min
                pts = np.array([[x1,y1,0],[x2,y2,0],[x3,y3,0],[x4,y4,0]])

                wm_col = cv2.cvtColor(display.world_hm,cv2.COLOR_GRAY2RGB)
                colours = [(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255),(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255),(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255),(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255),(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255),(0,0,255), (0,255,125), (255,0,0), (0,255,0), (125,0,255)]

                display.reset_markers()
                for i in range(1,num_workers+1):
                    if display.pos[i] is not None:
                        display.add_markers(i)
                        x_pos, y_pos, yaw = display.pos[i]['x']/display.grid_size, display.pos[i]['y']/display.grid_size, display.pos[i]['yaw']
                        # print(x_pos, y_pos)
                        rect_pts = transform_rot3d(yaw, [int(x_pos), int(y_pos), 0], pts)
                        # ? Why was I doing this?
                        # rect_pts = self.transform_rot3d(initial_yaw, [int(initial_pos[0]/self.grid_size), int(initial_pos[1]/self.grid_size), 0], rect_pts)
                        rect_pts = (rect_pts - display.world_offset)[:,:2]
                        rect_pts = np.array([[rect_pts[0][1], rect_pts[0][0]],[rect_pts[1][1], rect_pts[1][0]],[rect_pts[2][1], rect_pts[2][0]],[rect_pts[3][1], rect_pts[3][0]]])
                        cv2.polylines(wm_col, np.int32([rect_pts]), True, colours[i], 2)
                display.pub_markers()
                cv2.imshow("world", wm_col)
                cv2.waitKey(1)
                hms = np.zeros([im_size,num_workers*(im_size+40),1])
                # print([key for key in display.hms])
                # for i in range(1,num_workers+1):
                for i in range(0,num_workers):
                    hms[:im_size,i*(im_size+40):(i*(im_size+40)+im_size),:] = display.hms[i+1]
                cv2.imshow("heightmaps", hms)
                cv2.waitKey(1)

                # rospy.spin_once()
            except KeyboardInterrupt:
                print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    run()

# PKG = 'numpy_tutorial'
# import roslib; roslib.load_manifest(PKG)

# import rospy
# from rospy_tutorials.msg import Floats
# from rospy.numpy_msg import numpy_msg

# def callback(data):
#     # print (rospy.get_name(), "I heard %s"%str(data.data))

# def listener():
#     rospy.init_node('listener')
#     rospy.Subscriber("floats", numpy_msg(Floats), callback)
#     rospy.spin()

# if __name__ == '__main__':
#     listener()


# #!/usr/bin/env python
# PKG = 'numpy_tutorial'
# import roslib; roslib.load_manifest(PKG)

# import rospy
# from rospy.numpy_msg import numpy_msg
# from rospy_tutorials.msg import Floats

# import numpy
# def talker():
#     pub = rospy.Publisher('floats', numpy_msg(Floats),queue_size=10)
#     rospy.init_node('talker', anonymous=True)
#     r = rospy.Rate(10) # 10hz
#     while not rospy.is_shutdown():
#         a = numpy.array([1.0, 2.1, 3.2, 4.3, 5.4, 6.5], dtype=numpy.float32)
#         pub.publish(a)
#         r.sleep()

# if __name__ == '__main__':
#     talker()

# !/usr/bin/env python
# from __future__ import print_function

# import roslib
# roslib.load_manifest('my_package')
# import sys
# import rospy
# import cv2
# from std_msgs.msg import String
# from sensor_msgs.msg import Image
# from cv_bridge import CvBridge, CvBridgeError
# # import ros_numpy

# class image_converter:

#   def __init__(self):
#     self.image_pub = rospy.Publisher("image_topic_2",Image)

#     self.bridge = CvBridge()
#     self.image_sub = rospy.Subscriber("image_topic",Image,self.callback)

#   def callback(self,data):
#     try:
#       cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
#     except CvBridgeError as e:
#       print(e)

#     (rows,cols,channels) = cv_image.shape
#     if cols > 60 and rows > 60 :
#       cv2.circle(cv_image, (50,50), 10, 255)

#     cv2.imshow("Image window", cv_image)
#     cv2.waitKey(3)

#     try:
#       self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
#     except CvBridgeError as e:
#       print(e)

# def main(args):
#   ic = image_converter()
#   rospy.init_node('image_converter', anonymous=True)
#   try:
#     rospy.spin()
#   except KeyboardInterrupt:
#     print("Shutting down")
#   cv2.destroyAllWindows()

# if __name__ == '__main__':
#     main(sys.argv)