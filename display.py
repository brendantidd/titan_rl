'''
Script used to display robot stuff while training (loads in save robot state)
'''
import pybullet as p
import numpy as np
import argparse
from pathlib import Path
import time
home = str(Path.home())
from assets.obstacles_pb import Obstacles

obstacles = Obstacles()
import defaults
from dotmap import DotMap
args1, unknown1 = defaults.get_defaults() 
parser = argparse.ArgumentParser()
# Arguments that are specific for this run (including run specific defaults, ignore unknown arguments)
parser.add_argument('--render', default=True, action='store_false')
parser.add_argument('--robot', default='dynamic', action='store_true')
parser.add_argument('--multi_robot', default=False, action='store_true')
parser.add_argument('--horizon', default=2048, type=int)
parser.add_argument('--eval', default=False, action='store_true')
parser.add_argument('--tag', default="")
args2, unknown2 = parser.parse_known_args()
args2 = vars(args2)
# Replace any arguments from defaults with run specific defaults
for key in args2:
    args1[key] = args2[key]
# Look for any changes to defaults (unknowns) and replace values in args1
for n, unknown in enumerate(unknown2):
    if "--" in unknown and n < len(unknown2)-1 and "--" not in unknown2[n+1]:
        arg_type = type(args1[unknown[2:]])
        args1[unknown[2:]] = arg_type(unknown2[n+1])
args = DotMap(args1)
# Check for dodgy arguments
unknowns = []
for unknown in unknown1 + unknown2:
    if "--" in unknown and unknown[2:] not in args:
        unknowns.append(unknown)
if len(unknowns) > 0:
    print("Dodgy argument")
    print(unknowns)
    exit()

if args.hpc:
    hpc = 'hpc-home/'
        # MODEL_PATH = home + '/' + hpc + 'results/biped_model/weights/w6/' + args.exp + '/'
        # MODEL_PATH = home + '/' + hpc + 'results/biped_model/a11/' + args.exp + '/'
else:
    hpc = ''
    # MODEL_PATH = home + '/' + hpc + 'results/biped_model/weights/b10/base/'
MODEL_PATH = home + '/' + hpc + 'results/titan_rl/latest/' + args.exp + '/'
if args.tag != "":
    tail = "_" + args.tag
else:
    tail = args.tag
box_path = MODEL_PATH + 'box_info' + tail + '.npy'
path = MODEL_PATH + 'sim_data' + tail + '.npy'
# from assets.env_mocap import Env
from assets.env_pb import Env
env = Env(args=args)

def data_gen(path, box_path = None):
    print(path)
    # env.reset()
    if args.multi_robots:
        run_once = True
    num_runs = 0
    while True:
        try:
            try:
                box_info = np.load(box_path, allow_pickle=True)
            except Exception as e:
                print("loading stuff ", e)
                # pass
            sim_data = np.load(path, allow_pickle=True)
            try:
                if args.multi_robots and run_once and num_runs > 0:
                    while True:
                        pass
                print("reseting env")
                env.reset(box_info=box_info)

                # obstacles.remove_obstacles()
                # # box_info = np.load(box_path, allow_pickle=True)
                # for pos, size, colour in zip(box_info[1], box_info[2], box_info[3]):
                #     # print(pos[2], size[2])
                #     # print(pos[3:])
                #     obstacles.add_box(pos=pos[:3], orn=pos[3:], size=size, colour=colour)
            except Exception as e:
                print("box stuff ", e)
            if len(sim_data) == 0:
                yield None
            count = 0
            num_runs += 1
            for seg in sim_data:
                yield seg, count
                count += 1
            print(count)
        except Exception as e:
                print("exception")
                print(e)

if args.eval:
    data = data_gen(path = MODEL_PATH + 'sim_data_eval.npy', box_path = MODEL_PATH + 'box_info_eval.npy')
elif args.long:
    data = data_gen(path = MODEL_PATH + 'sim_data_eval_9000.npy', box_path = MODEL_PATH + 'box_info_eval_9000.npy')
else:
    # data = data_gen(path = MODEL_PATH + 'sim_data.npy', box_path = MODEL_PATH + 'box_info.npy')
    data = data_gen(path = path, box_path = box_path)
if args.mocap:
    exp_data = data_gen(path = MODEL_PATH + 'exp_sim_data.npy')

while True:
    time.sleep(0.1)
    seg, count = data.__next__()
    if count == 0:
        prev_box_num = 0
        freeze_next = False
        freeze_box = 0
        # locations = [1,2,3,4]
        # locations = [150,200,240,280,330,350]
        # locations = [140, 240, 280,320, 350, 390]
        # locations = [140, 240, 290,340, 360, 390]
        locations = [140, 240, 290,340, 380]
    if seg is not None:
        # print(env.box_num, prev_box_num)
        # if env.box_num != prev_box_num:
        # print(seg)
        env.step(actions=np.zeros(env.ac_size), set_position=seg)
        # else:
            # env.step(set_position=seg)
        # if args.multi_robots and env.box_num != prev_box_num:
        # if args.multi_robots and env.body_xyz[0] > env.box_info[1][freeze_box][0]:
        # if args.multi_robots and freeze_box < len(locations) and env.body_xyz[0] > locations[freeze_box]:
        if args.multi_robots and freeze_box < len(locations) and env.steps > locations[freeze_box]:
            freeze_next = True
            freeze_box += 1
        else:
            freeze_next = False
        # prev_box_num = env.box_num
        # env.set_position(seg[0], seg[1], seg[2])
        # env.get_observation()
        # env.actions = np.zeros(env.ac_size)
        # env.order = 
        # env.get_reward()
        # print(count)
    else:
        env.set_position([0, 0, -0.045], [0,0,0,1], np.zeros(12))
#     if args.mocap:
        # exp_seg, exp_count = exp_data.__next__()
        # if exp_seg is not None:
        #     env.set_position(exp_seg[0], exp_seg[1], exp_seg[2], robot_id=env.exp_Id)
        # else:
        #     env.set_position([0, 1, -0.045], [0,0,0,1], np.zeros(12), robot_id=env.exp_Id)
    # p.stepSimulation()
    # if args.render:
        # time.sleep(0.02)
