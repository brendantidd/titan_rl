import warnings
warnings.filterwarnings('ignore')
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False
# import tensorflow.python.util.deprecation as deprecation
# deprecation._PRINT_DEPRECATION_WARNINGS = False
# import tensorflow as tf
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
# if type(tf.contrib) != type(tf): tf.contrib._warning = None
# tf.compat.v1.logging.info('TensorFlow')
# # INFO:tensorflow:TensorFlow
# tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
# tf.compat.v1.logging.info('TensorFlow')

# import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow import nn
import tensorboardX
import numpy as np
import time
import scripts.utils as U
from baselines.common.mpi_running_mean_std import RunningMeanStd
import argparse
from pathlib import Path
home = str(Path.home())
from scripts.utils import subplot
import random
from rand_heightmap_generator import HeightMapGenerator
from collections import deque
import cv2

class GapDetector():
    # def __init__(self, args, name = 'gaps', im_size=[80,80,1], label_size=6, dense_size=64, sess=None, train_pol=True):
    def __init__(self, args, name = 'gap_detector', im_size=[80,80,1], label_size=6, dense_size=32, sess=None, train_pol=True):

        self.name = name
        self.args = args
        self.train_pol = train_pol
        self.im_size = im_size
        self.label_size = label_size
        self.dense_size = dense_size
        self.sess = tf.get_default_session() if sess is None else sess
        if self.args.include_waypoints:
            self.points = tf.compat.v1.placeholder(tf.float32, [None, 4])
        self.im = tf.compat.v1.placeholder(tf.float32, [None] + self.im_size)
        self.labels = tf.compat.v1.placeholder(tf.float32, [None, self.label_size])
        with tf.compat.v1.variable_scope(self.name):
            self.model()
        var = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name+"/")
        self.vars = [v for v in var if 'RMSProp' not in v.name or 'Adam' not in v.name]
        # print(self.vars)
        self.saver = tf.compat.v1.train.Saver(var_list=self.vars)

    def save(self, SAVE_PATH):
        self.saver.save(self.sess, SAVE_PATH + 'gap_detector.ckpt', write_meta_graph=False)
    
    def load(self, WEIGHT_PATH):
        model_name = 'gap_detector.ckpt'
        self.saver.restore(self.sess, WEIGHT_PATH + model_name)
        print("Loaded weights for gaps module")
        
    def model(self):
        # last_out = tf.nn.relu(U.conv2d(self.im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
        # last_out = tf.nn.relu(U.conv2d(last_out, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))

        last_out = tf.nn.relu(U.conv2d(self.im, 32, "vis_l1", [4, 4], [2, 2], pad="VALID"))
        last_out = tf.nn.relu(U.conv2d(last_out, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))

        last_out = U.flattenallbut0(last_out)

        if self.args.include_waypoints:
            last_out = tf.concat(axis=1,values=[last_out, self.points])

        last_out = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin1', kernel_initializer=U.normc_initializer(1.0)))

        last_out = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin3', kernel_initializer=U.normc_initializer(1.0)))

        self.z = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin2', kernel_initializer=U.normc_initializer(1.0)))
        # self.z = tf.nn.tanh(tf.layers.dense(last_out, self.dense_size, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
        # self.z = tf.nn.relu(tf.layers.dense(last_out, self.dense_size, name='vis_lin', kernel_initializer=U.normc_initializer(0.01)))
        self.door_bit = tf.sigmoid(tf.layers.dense(self.z, 1, name="l4"))
        self.other_bits = tf.layers.dense(self.z, self.label_size-1, name="l5")
        self.outputs = tf.concat(axis=1,values=[self.door_bit, self.other_bits])
        
        self.loss = tf.reduce_mean(tf.squared_difference(self.outputs, self.labels)) 
        optimizer = tf.train.AdamOptimizer(learning_rate=self.args.gaps_learning_rate)
        self.optimise = optimizer.minimize(self.loss)

    def step(self, im, points=None):
        if points is not None:
            z, outputs = self.sess.run([self.z, self.outputs],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size), self.points:np.array(points).reshape([-1,4])})
        else:
            z, outputs = self.sess.run([self.z, self.outputs],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size)})
        return z[0], outputs[0]

    def multi_step(self, im, label, points=None):
        if points is not None:
            outputs, loss = self.sess.run([self.outputs, self.loss],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(label).reshape(-1,self.label_size), self.points:np.array(points).reshape([-1,4])})
        else:
            outputs, loss = self.sess.run([self.outputs, self.loss],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(label).reshape(-1,self.label_size)})
        return outputs, loss

    def train(self, im, label, points=None):
        if points is not None:
            _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(label).reshape(-1,self.label_size), self.points:np.array(points).reshape([-1,4])})
        else:
            _, loss = self.sess.run([self.optimise, self.loss],feed_dict={self.im:np.array(im).reshape([-1] + self.im_size), self.labels:np.array(label).reshape(-1,self.label_size)})
        return loss


    def initialise(self):
        self.sess.run(tf.variables_initializer(self.vars))


class DataBuffer():
    def __init__(self, im_size= [80,80,1], label_size=6, size=10000):
        self.im_size = im_size
        self.label_size = label_size
        self.imgs = np.zeros([size] + im_size, dtype=np.float32)
        self.labels = np.zeros([size, label_size], dtype=np.float32)
        self.points = np.zeros([size, 4], dtype=np.float32)
        # self.val_imgs = np.zeros(im_size)
        # self.val_labels = np.zeros(label_size)
        self.pointer = 0
        self.full = False
        self.size = size
        # self.val_pointer = 0
        # self.val_full = False
            
    def add(self, imgs, labels, points):
        for img, label, point in zip(imgs, labels, points):
        # self.pointer
        # self.pointer += self.imgs.shape[0]
        # self.val_pointer += self.val_imgs.shape[0]
        # if self.pointer > self.size:
        #     self.pointer = 0
        #     self.full = True
        # if self.val_pointer > self.size:
        #     self.val_pointer = 0
        #     self.val_full = True
            self.imgs[self.pointer] = img
            self.labels[self.pointer] = label
            self.points[self.pointer] = point
            # self.val_imgs[self.pointer] = val_imgs
            # self.val_labels[self.pointer] = val_labels
            self.pointer += 1
            if self.pointer >= self.size:
                self.pointer = 0
                self.full = True

    def sample(self, batch_size):
        if not self.full:
            idx = random.sample([i for i in range(self.pointer)], batch_size)
        else:
            idx = random.sample([i for i in range(self.size)], batch_size)
        return self.imgs[idx], self.labels[idx], self.points[idx]

# def get_data(hm_gen, DATA_PATH, data_size=150000):
def get_data(hm_gen, DATA_PATH, data_size=200000):
# def get_data(hm_gen, DATA_PATH, data_size=1000):
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    num_workers = comm.Get_size()
    rank = comm.Get_rank()

    batch_size = int(data_size/num_workers)

    if rank == 0:
        print("get data ", data_size, " validation ", int(data_size*0.1), " from each worker ", batch_size, " validation ", int(batch_size*0.1))
    
    t1 = time.time()    
    im, labels, points = hm_gen.get_batch(batch_size=batch_size)

    if rank == 0:
        print("got data in ", time.time() - t1," now getting val data")

    val_im, val_labels, val_points = hm_gen.get_batch(batch_size=int(batch_size*0.1))
    
    if rank == 0:
        # print(np.array(im).shape, np.array(labels).shape, np.array(points).shape, np.array(val_im).shape, np.array(val_labels).shape, np.array(val_points).shape)
        print(rank, np.array(im).shape, np.array(labels).shape, np.array(points).shape, np.array(val_im).shape, np.array(val_labels).shape, np.array(val_points).shape,"time ", time.time() - t1 )
    np.save(DATA_PATH + "imgs" + str(rank) + ".npy", np.array(im))
    np.save(DATA_PATH + "labels" + str(rank) + ".npy", np.array(labels))
    np.save(DATA_PATH + "points" + str(rank) + ".npy", np.array(points))
    np.save(DATA_PATH + "val_imgs" + str(rank) + ".npy", np.array(val_im))
    np.save(DATA_PATH + "val_labels" + str(rank) + ".npy", np.array(val_labels))
    np.save(DATA_PATH + "val_points" + str(rank) + ".npy", np.array(val_points))
    if rank == 0:
        print("saving to ", DATA_PATH)

def load_data(DATA_PATH):
    print("loading from ", DATA_PATH)
    from glob import glob
    import re
    imgs = []
    labels = []
    points = []
    val_imgs = []
    val_labels = []
    val_points = []
    max_i = 0
    for npy_file in glob(DATA_PATH + '/*.npy'):
        # print(npy_file)
        numbers = re.findall(r'\d+', npy_file)
        if int(numbers[-1]) > max_i:
            max_i = int(numbers[-1])
    print("max number from data file = ", max_i)
    for i in range(max_i + 1):
        imgs.append(np.load(DATA_PATH + "imgs" + str(i) + ".npy"))
        labels.append(np.load(DATA_PATH + "labels" + str(i) + ".npy"))
        points.append(np.load(DATA_PATH + "points" + str(i) + ".npy"))
        val_imgs.append(np.load(DATA_PATH + "val_imgs" + str(i) + ".npy"))
        val_labels.append(np.load(DATA_PATH + "val_labels" + str(i) + ".npy"))
        val_points.append(np.load(DATA_PATH + "val_points" + str(i) + ".npy"))
    print(np.concatenate(imgs).shape, np.concatenate(labels).shape, np.concatenate(points).shape, np.concatenate(val_imgs).shape, np.concatenate(val_labels).shape, np.concatenate(val_points).shape)
    return np.concatenate(imgs), np.concatenate(labels), np.concatenate(points), np.concatenate(val_imgs), np.concatenate(val_labels), np.concatenate(val_points)

if __name__=="__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--folder', default="gaps")
    parser.add_argument('--exp', default="test")
    parser.add_argument('--include_waypoints', default=True, action="store_false")
    parser.add_argument('--get_data', default=False, action="store_true")
    parser.add_argument('--gaps_learning_rate', default=0.0001, type=float)
    # parser.add_argument('--gaps_learning_rate', default=0.001, type=float)
    args = parser.parse_args()

    # os.environ["CUDA_VISIBLE_DEVICES"]="-1"


    PATH = home + '/results/titan_rl/latest/' + args.folder + '/' + args.exp + '/'
    # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '6/'
    # WEIGHTS_PATH = home + '/results/biped_model/latest/' + args.folder + '/data/' + args.obstacle_type + '6/'


    writer = tensorboardX.SummaryWriter(log_dir=PATH)


    DATA_PATH = home + '/results/titan_rl/latest/' + args.folder + '/data3/'
    hm_gen = HeightMapGenerator()
    if args.get_data:
        writer = tensorboardX.SummaryWriter(log_dir=DATA_PATH)
        get_data(hm_gen, DATA_PATH)
        exit()
    else:
        imgs, labels, points, val_imgs, val_labels, val_points = load_data(DATA_PATH)

    # gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction= 0.15)
    gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction= 0.65)
    # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 0.2)
    # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction= 1.0)
    sess = tf.compat.v1.InteractiveSession(config=tf.compat.v1.ConfigProto(inter_op_parallelism_threads=1,
                                                                                    intra_op_parallelism_threads=1,                         
                                                                                    gpu_options=gpu_options), graph=None)   

    
    net = GapDetector(args=args, sess=sess)
    net.initialise()
    
    batch_size = 1024
    epochs = 200
    radius = 2
    val_num = 128
    best_loss = 1000

    idx = [i for i in range(imgs.shape[0])]
    for e in range(epochs):
        np.random.shuffle(idx)
        # print(idx[:10])
        t1 = time.time()
        epoch_loss = []
        for batch in range(len(idx)//batch_size):
            b_idx = batch*batch_size
            loss = net.train(imgs[idx[b_idx:b_idx+batch_size],:], labels[idx[b_idx:b_idx+batch_size],:], points[idx[b_idx:b_idx+batch_size],:])
            epoch_loss.append(loss)
    
        val_idx = random.sample(range(val_imgs.shape[0]), 2056)
        val_preds, val_loss = net.multi_step(val_imgs[val_idx], val_labels[val_idx], val_points[val_idx])
        # val_idx = random.sample(range(val_imgs.shape[0]), val_num)
        if np.mean(val_loss) < best_loss:
            best_loss = np.mean(val_loss)
            net.save(PATH + '/best/')
            col_val_imgs = []
            for label, pred, img, point in zip(val_labels[val_idx[:val_num]], val_preds[:val_num], val_imgs[val_idx[:val_num]], val_points[val_idx[:val_num]]):
                col_img = cv2.cvtColor(img*255.0,cv2.COLOR_GRAY2RGB)
                cv2.rectangle(col_img, (0, 0), (img.shape[0], img.shape[1]), (255,255,255), 1)                

                # label = [0,angle, dist, _,_]
                if label[0]:
                    x, y = label[3]*np.cos(label[2]), label[3]*np.sin(label[2])
                    x1, y1 = label[5]*np.cos(label[4]), label[5]*np.sin(label[4])
                    image_x, image_y = hm_gen.robot_to_image([x, y], img.shape)
                    cv2.circle(col_img, (image_y, image_x), int(radius/0.1), (0,255,0))
                    image_x1, image_y1 = hm_gen.robot_to_image([x1, y1], img.shape)
                    cv2.circle(col_img, (image_y1, image_x1), int(radius/0.1), (0,255,0))
                # Add points. 
                point_x1, point_y1 = hm_gen.robot_to_image([point[0], point[1]], img.shape)
                cv2.circle(col_img, (point_y1, point_x1), 1, (0,255,0))
                point_x2, point_y2 = hm_gen.robot_to_image([point[2], point[3]], img.shape)
                cv2.circle(col_img, (point_y2, point_x2), 1, (0,255,0))

                    
                if pred[0] > 0.5:
                    x, y = pred[3]*np.cos(pred[2]), pred[3]*np.sin(pred[2])
                    x1, y1 = pred[5]*np.cos(pred[4]), pred[5]*np.sin(pred[4])
                    image_x, image_y = hm_gen.robot_to_image([x, y], img.shape)
                    cv2.circle(col_img, (image_y, image_x), int(radius/0.1), (0,0,255))
                    image_x1, image_y1 = hm_gen.robot_to_image([x1, y1], img.shape)
                    cv2.circle(col_img, (image_y1, image_x1), int(radius/0.1), (0,0,255))
                
                col_val_imgs.append(col_img)

            
            # col_val_imgs = np.stack([[col_val_imgs[:int(len(col_val_imgs)/2)]],
            #                          [col_val_imgs[int(len(col_val_imgs)/2):]]]).reshape([int(len(col_val_imgs)/2)*80, 2*80,3]).astype(np.uint8)

            # print(col_val_imgs.shape, col_val_imgs.dtype)
            if len(col_val_imgs) > 0:
                # print(len(col_val_imgs))
                col_val_imgs1 = np.hstack(col_val_imgs[:int(val_num/4)]).astype(np.uint8)
                col_val_imgs2 = np.hstack(col_val_imgs[int(val_num/4):int(2*val_num/4)]).astype(np.uint8)
                col_val_imgs3 = np.hstack(col_val_imgs[int(2*val_num/4):int(3*val_num/4)]).astype(np.uint8)
                col_val_imgs4 = np.hstack(col_val_imgs[int(3*val_num/4):]).astype(np.uint8)
                # , np.hstack(col_val_imgs[len(col_val_imgs)/2:]).astype(np.uint8)])
                # col_val_imgs = col_val_imgs1
                col_val_imgs = np.vstack([col_val_imgs1,col_val_imgs2, col_val_imgs3, col_val_imgs4])
                # col_val_imgs = np.stack([[col_val_imgs[:len(col_val_imgs)/2]], [col_val_imgs[len(col_val_imgs)/2:]]]).astype(np.uint8)
                cv2.imshow('frame', col_val_imgs)
                cv2.waitKey(1)
                cv2.imwrite(PATH + '/img' + str(e) + '.png', col_val_imgs)

        if e % 10 == 0:
            net.save(PATH)  

        # print("Epoch {0:d} Loss {1:.4f} Val Loss {2:.4f} Data gen time {3:.4f} Time {4:.4f} Best loss {5:.4f}".format(e, np.mean(epoch_loss), np.mean(val_loss),  t_data_gen,  time.time() - t1, best_loss))
        print("Epoch {0:d} Loss {1:.4f} Val Loss {2:.4f} Time {3:.4f} Best loss {4:.4f}".format(e, np.mean(epoch_loss), np.mean(val_loss),  time.time() - t1, best_loss))
        # writer = tensorboardX.SummaryWriter(log_dir=PATH)
        writer.add_scalar("loss", np.mean(epoch_loss), e)
        writer.add_scalar("loss_val", np.mean(val_loss), e)
        writer.add_scalar("time_per_epoch", time.time() - t1, e)



    # # old
    # batch_size = 256
    # epoch_size = 10 #train batch_size epoch_size times
    # # epochs = 500
    # epochs = 1000
    # best_loss = 1000
    # # imgs = deque(maxlen=10000)
    # # labels = deque(maxlen=10000)
    # # val_imgs = deque(maxlen=1000)
    # # val_labels = deque(maxlen=1000)

    # # imgs = np.zeros()

    # # def get_data(imgs, labels, val_imgs, val_labels):
    # # def get_data():
    # #     img, label = hm_gen.get_batch(batch_size=512)
    # #     val_img, val_label = hm_gen.get_batch(batch_size=64)
    # #     # imgs.append(img)
    # #     # labels.append(label)
    # #     # val_imgs.append(val_img)
    # #     # val_labels.append(val_label)
    # #     # print(len(imgs), len(val_imgs))
    # #     return img, label, val_img, val_label
    
    # data_buffer = DataBuffer(size=50000)
    # val_buffer = DataBuffer(size=50000)
    # # Must add one at a time
    #     # img, label = hm_gen.get_batch(batch_size=512)
    # print("Adding to buffer")
    # t1 = time.time()
    # # data_buffer.add(*hm_gen.get_batch(batch_size=1000))
    # # val_buffer.add(*hm_gen.get_batch(batch_size=128))
    # data_buffer.add(*hm_gen.get_batch(batch_size=20000))
    # val_buffer.add(*hm_gen.get_batch(batch_size=2000))
    # # data_buffer.add(*hm_gen.get_batch(batch_size=512))
    # # val_buffer.add(*hm_gen.get_batch(batch_size=128))
    # print("Buffer has stuff in it: ", time.time()-t1)


    # # for _ in range(100):
    # # for _ in range(10):
    # #     get_data(imgs, labels, val_imgs, val_labels)
    # #     print(len(imgs), len(val_imgs))
    # radius = 2
    # print("Starting training")
    # for e in range(epochs):
    #     # if e % 10 == 0:
    #     t1 = time.time()
    #     epoch_loss = []
    #     t_data_gen = 0
    #     t2 = time.time()
    #     # imgs, labels, val_imgs, val_labels = get_data(imgs, labels, val_imgs, val_labels)
    #     # imgs, labels, val_imgs, val_labels = get_data()
    #     data_buffer.add(*hm_gen.get_batch(batch_size=50))
    #     val_buffer.add(*hm_gen.get_batch(batch_size=5))
    #     t_data_gen += time.time() - t2
    #     # print("got batch time: ", time.time() - t2)
    #     for _ in range(1):
    #         imgs, labels, points = data_buffer.sample(1000)
    #         loss = net.train(imgs, labels, points)
    #         epoch_loss.append(loss) 

    #     val_num = 128
    #     val_imgs, val_labels, val_points = val_buffer.sample(val_num)
    #     val_preds, val_loss = net.multi_step(val_imgs, val_labels, val_points)
    #     if np.mean(val_loss) < best_loss:
    #         best_loss = np.mean(val_loss)
    #         net.save(PATH + '/best/')
    #         col_val_imgs = []
    #         for label, pred, img, point in zip(val_labels, val_preds, val_imgs, val_points):
    #             col_img = cv2.cvtColor(img*255.0,cv2.COLOR_GRAY2RGB)
    #             cv2.rectangle(col_img, (0, 0), (img.shape[0], img.shape[1]), (255,255,255), 1)                

    #             # label = [0,angle, dist, _,_]
    #             if label[0]:
    #                 x, y = label[3]*np.cos(label[2]), label[3]*np.sin(label[2])
    #                 x1, y1 = label[5]*np.cos(label[4]), label[5]*np.sin(label[4])
    #                 image_x, image_y = hm_gen.robot_to_image([x, y], img.shape)
    #                 cv2.circle(col_img, (image_y, image_x), int(radius/0.1), (0,255,0))
    #                 image_x1, image_y1 = hm_gen.robot_to_image([x1, y1], img.shape)
    #                 cv2.circle(col_img, (image_y1, image_x1), int(radius/0.1), (0,255,0))
    #                 point_x1, point_y1 = hm_gen.robot_to_image([point[0], point[1]], img.shape)
    #                 cv2.circle(col_img, (point_y1, point_x1), 1, (0,255,0))
    #                 point_x2, point_y2 = hm_gen.robot_to_image([point[2], point[3]], img.shape)
    #                 cv2.circle(col_img, (point_y2, point_x2), 1, (0,255,0))

                    
    #             if pred[0] > 0.5:
    #                 x, y = pred[3]*np.cos(pred[2]), pred[3]*np.sin(pred[2])
    #                 x1, y1 = pred[5]*np.cos(pred[4]), pred[5]*np.sin(pred[4])
    #                 image_x, image_y = hm_gen.robot_to_image([x, y], img.shape)
    #                 cv2.circle(col_img, (image_y, image_x), int(radius/0.1), (0,0,255))
    #                 image_x1, image_y1 = hm_gen.robot_to_image([x1, y1], img.shape)
    #                 cv2.circle(col_img, (image_y1, image_x1), int(radius/0.1), (0,0,255))
                
    #             col_val_imgs.append(col_img)

            
    #         # col_val_imgs = np.stack([[col_val_imgs[:int(len(col_val_imgs)/2)]],
    #         #                          [col_val_imgs[int(len(col_val_imgs)/2):]]]).reshape([int(len(col_val_imgs)/2)*80, 2*80,3]).astype(np.uint8)

    #         # print(col_val_imgs.shape, col_val_imgs.dtype)
    #         if len(col_val_imgs) > 0:
    #             # print(len(col_val_imgs))
    #             col_val_imgs1 = np.hstack(col_val_imgs[:int(val_num/4)]).astype(np.uint8)
    #             col_val_imgs2 = np.hstack(col_val_imgs[int(val_num/4):int(2*val_num/4)]).astype(np.uint8)
    #             col_val_imgs3 = np.hstack(col_val_imgs[int(2*val_num/4):int(3*val_num/4)]).astype(np.uint8)
    #             col_val_imgs4 = np.hstack(col_val_imgs[int(3*val_num/4):]).astype(np.uint8)
    #             # , np.hstack(col_val_imgs[len(col_val_imgs)/2:]).astype(np.uint8)])
    #             # col_val_imgs = col_val_imgs1
    #             col_val_imgs = np.vstack([col_val_imgs1,col_val_imgs2, col_val_imgs3, col_val_imgs4])
    #             # col_val_imgs = np.stack([[col_val_imgs[:len(col_val_imgs)/2]], [col_val_imgs[len(col_val_imgs)/2:]]]).astype(np.uint8)
    #             cv2.imshow('frame', col_val_imgs)
    #             cv2.waitKey(1)
    #             cv2.imwrite(PATH + '/img' + str(e) + '.png', col_val_imgs)

    #     if e % 10 == 0:
    #         net.save(PATH)  

    #     print("Epoch {0:d} Loss {1:.4f} Val Loss {2:.4f} Data gen time {3:.4f} Time {4:.4f} Best loss {5:.4f}".format(e, np.mean(epoch_loss), np.mean(val_loss),  t_data_gen,  time.time() - t1, best_loss))
    #     # writer = tensorboardX.SummaryWriter(log_dir=PATH)
    #     writer.add_scalar("loss", np.mean(epoch_loss), e)
    #     writer.add_scalar("loss_val", np.mean(val_loss), e)
    #     writer.add_scalar("time_per_epoch", time.time() - t1, e)

    #     # for label, pred, img in zip(val_labels, val_preds, val_imgs):
    #         # hm_gen.plot(img, lab)
    #         # print(label, pred)
        



        
    #     # test_equal_idx = random.sample([_ for _ in range(test_bigger.shape[0])], test_smaller.shape[0])
    #     # test_idx = list(test_smaller) + list(test_bigger[test_equal_idx])     
    #     # # print(len(test_idx), len(test_smaller), len(test_bigger))
    #     # np.random.shuffle(test_idx)
     
    #     # val_loss, predictions = sess.run([net.loss, net.outputs],feed_dict={net.inputs:test_obs[test_idx,:], net.im:test_imgs[test_idx,:], net.labels:test_labels[test_idx,:]})
    #     # # val_loss, predictions = sess.run([net.loss, net.outputs],feed_dict={net.inputs:test_obs, net.labels:test_labels})
    #     # # print(predictions)
    #     # # -----------------------------------------------------------
    #     # # Display epoch losses. Save best weights, periodically plot things
    #     # # -----------------------------------------------------------
    #     # print("Epoch {0:d} Loss {1:.4f} Val_loss {2:.4f} Time {3:.4f} Best val {4:.4f}".format(e, np.mean(epoch_loss),    np.mean(val_loss), time.time() - t1, best_val_loss))
    #     # writer.add_scalar("error", np.mean(epoch_loss), e)
    #     # writer.add_scalar("val error", np.mean(val_loss), e)
    #     # if np.mean(val_loss) < best_val_loss:
    #     #     net.save(PATH + '/best/')
    #     #     best_val_loss = np.mean(val_loss)
    #     #     best_count += 1
    #     #     # if np.random.random() < 0.2:
    #     #     if e > 100 and best_count % 10 == 0:
    #     #         rand_idx = test_idx[:128]
    #     #         y1 = test_labels[rand_idx,0]
    #     #         k1 = predictions[:128]
    #     #         subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH + '/best/')
    #     # if e % 10 == 0:
    #     #     net.save(PATH)            
    #     # if e % 200 == 0:
    #     #     rand_idx = test_idx[:128]
    #     #     y1 = test_labels[rand_idx,0]
    #     #     k1 = predictions[:128]    
    #     #     subplot([[y1,k1],[y1,k1]], legend=[['target','predictions']]*2,PATH=PATH)
    
