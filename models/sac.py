from models.base import Base
import os   
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
tf.get_logger().setLevel('DEBUG')
from scripts.mpi_running_mean_std import RunningMeanStd
from scripts.mpi_moments import mpi_moments
# from baselines.common import explained_variance, fmt_row, zipsame
import numpy as np
from gym import spaces
# import scripts.utils as U
import scripts.utils as TF_U
from scripts.mpi_adam_optimizer import MpiAdamOptimizer
from mpi4py import MPI
from scripts import logger
import time

def gaussian_likelihood(x, mu, log_std):
    pre_sum = -0.5 * (((x-mu)/(tf.exp(log_std)+1e-8))**2 + 2*log_std + np.log(2*np.pi))
    return tf.reduce_sum(pre_sum, axis=1)

def apply_squashing_func(mu, pi, logp_pi):
    # Adjustment to log prob
    # NOTE: This formula is a little bit magic. To get an understanding of where it
    # comes from, check out the original SAC paper (arXiv 1801.01290) and look in
    # appendix C. This is a more numerically-stable equivalent to Eq 21.
    # Try deriving it yourself as a (very difficult) exercise. :)
    logp_pi -= tf.reduce_sum(2*(np.log(2) - pi - tf.nn.softplus(-2*pi)), axis=1)

    # Squash those unbounded actions!
    mu = tf.tanh(mu)
    pi = tf.tanh(pi)
    return mu, pi, logp_pi

def get_vars(scope):
    return [x for x in tf.global_variables() if scope in x.name]

class Policy():
    def __init__(self, ob, im, ob_space, ob_size, im_size, ac_size, ac, sess, vis,  args, hid_size, normalize=True, const_std=False, max_action=5, ob_rms=None):
        self.ob = ob
        self.im = im
        self.ac = ac
        self.ob_size = ob_size
        self.im_size = im_size
        self.ac_size = ac_size
        self.sess = sess
        self.args = args
        self.vis = vis
        self.const_std = const_std
        self.max_action = max_action
        sequence_length = None

        self.ob_rms = ob_rms
        with tf.variable_scope("vis"):  
            x = tf.nn.relu(TF_U.conv2d(im, 16, "vis_l1", [8, 8], [4, 4], pad="VALID"))
            x = tf.nn.relu(TF_U.conv2d(x, 32, "vis_l2", [4, 4], [2, 2], pad="VALID"))
            x = TF_U.flattenallbut0(x)
            x = tf.nn.relu(tf.layers.dense(x, 64, name='vis_lin', kernel_initializer=TF_U.normc_initializer(1.0)))
                
        if normalize:
            obz = tf.clip_by_value((ob - tf.stop_gradient(self.ob_rms.mean)) / tf.stop_gradient(self.ob_rms.std), -5.0, 5.0)
        else:
            obz = ob
        
        with tf.variable_scope('q1'):
            last_out = tf.concat(axis=1,values=[obz, x, ac])
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
            self.q1 = tf.layers.dense(last_out, 1, name='final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]
        
        with tf.variable_scope('q2'):
            last_out = tf.concat(axis=1,values=[obz, x, ac])
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name="fc1", kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name="fc2", kernel_initializer=TF_U.normc_initializer(1.0)))
            self.q2 = tf.layers.dense(last_out, 1, name='final', kernel_initializer=TF_U.normc_initializer(1.0))[:,0]

        with tf.variable_scope('pi'):
            last_out = tf.concat(axis=1,values=[obz, x])
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name='fc1', kernel_initializer=TF_U.normc_initializer(1.0)))
            last_out = tf.nn.relu(tf.layers.dense(last_out, hid_size, name='fc2', kernel_initializer=TF_U.normc_initializer(1.0)))
            mu = tf.layers.dense(last_out, self.ac_size, name='final', kernel_initializer=TF_U.normc_initializer(0.01))
            
            log_std = tf.layers.dense(last_out, self.ac_size, activation=None)
            log_std = tf.clip_by_value(log_std, -20, 2)
            std = tf.exp(log_std)
            pi = mu + tf.random_normal(tf.shape(mu)) * std
            logp_pi = gaussian_likelihood(pi, mu, log_std)

            self.mu, self.pi, self.logp_pi = apply_squashing_func(mu, pi, logp_pi)
            self.mu *= self.max_action
            self.pi *= self.max_action

class Model(Base):
    def __init__(self, name, env, ac_size, ob_size, im_size=[48,48,4], args=None, PATH=None, writer=None, hid_size=256, vis=False, normalize=True, ent_coef=0.0, vf_coef=0.5, max_grad_norm=0.5, mpi_rank_weight=1, max_timesteps=int(1e6), lr=1e-3, horizon=2048, batch_size=100, const_std=False, max_action=5):
        self.max_timesteps = max_timesteps
        self.horizon = horizon
        self.batch_size = batch_size
        self.learning_rate = lr
        self.env = env
        self.ac_size = ac_size
        self.ob_size = ob_size
        self.im_size = im_size
        self.sess = sess = TF_U.get_session()
        comm = MPI.COMM_WORLD
        self.name = name
        high = np.inf*np.ones(ob_size)
        low = -high
        ob_space = spaces.Box(low, high, dtype=np.float32)
        self.args = args   
        im_size = im_size
        self.PATH = PATH
        self.hid_size = hid_size
        self.writer = writer
        
        self.args = args
        
        self.ac = tf.placeholder(tf.float32, [None, self.ac_size])
        self.ob = tf.placeholder(tf.float32, [None, self.ob_size])
        self.ob2 = tf.placeholder(tf.float32, [None, self.ob_size])
        self.im = tf.placeholder(tf.float32, [None] + self.im_size)
        self.im2 = tf.placeholder(tf.float32, [None] + self.im_size)
        self.rew = tf.placeholder(tf.float32, [None])
        self.done = tf.placeholder(tf.float32, [None])
        self.LR = tf.placeholder(tf.float32, [])

        if ('vel' in name or 'hex' in name) and not args.vis:
            self.vis = False
        else:
            self.vis = vis
                
        with tf.variable_scope(self.name):
            with tf.variable_scope("obfilter"):
                self.ob_rms = RunningMeanStd(shape=ob_space.shape)

            with tf.variable_scope('main'):
                self.pol = self.Policy(self.ob, self.im, ob_space, ob_size, im_size, ac_size, self.ac, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std, max_action=max_action, ob_rms=self.ob_rms)
                self.mu, self.pi, logp_pi, q1, q2 = self.pol.mu, self.pol.pi, self.pol.logp_pi, self.pol.q1, self.pol.q2
            
            with tf.variable_scope('main', reuse=True):
                # pi instead of self.ac
                pol2  = Policy(self.ob, self.im, ob_space, ob_size, im_size, ac_size, self.pi, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std, max_action=max_action, ob_rms=self.ob_rms)
                q1_pi, q2_pi = pol2.q1, pol2.q2

                pol_next = Policy(self.ob2, self.im2, ob_space, ob_size, im_size, ac_size, self.ac, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, 
                const_std=const_std, max_action=max_action, ob_rms=self.ob_rms)
                pi_next, logp_pi_next = pol_next.pi, pol_next.logp_pi

            with tf.variable_scope('target'):
                pol_target = Policy(self.ob2, self.im2, ob_space, ob_size, im_size, ac_size, pi_next, sess, self.vis, args=args, normalize=normalize, hid_size=hid_size, const_std=const_std, max_action=max_action, ob_rms=self.ob_rms)
                q1_targ, q2_targ = pol_target.q1, pol_target.q2

        Base.__init__(self)

        min_q_pi = tf.minimum(q1_pi, q2_pi)
        min_q_targ = tf.minimum(q1_targ, q2_targ)

        gamma = 0.99
        alpha = 0.2
        polyak=0.995
        
        # Entropy-regularized Bellman backup for Q functions, using Clipped Double-Q targets
        q_backup = tf.stop_gradient(self.rew + gamma*(1-self.done)*(min_q_targ - alpha * logp_pi_next))

        # Soft actor-critic losses
        pi_loss = tf.reduce_mean(alpha * logp_pi - min_q_pi)
        q1_loss = 0.5 * tf.reduce_mean((q_backup - q1)**2)
        q2_loss = 0.5 * tf.reduce_mean((q_backup - q2)**2)
        value_loss = q1_loss + q2_loss

        params = tf.trainable_variables(self.name + '/main/pi')
        # 2. Build our trainer
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

        # 3. Calculate the gradients
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            grads_and_var = self.trainer.compute_gradients(pi_loss, params)
            grads, var = zip(*grads_and_var)
        
        grads, _grad_norm = tf.clip_by_global_norm(grads, max_grad_norm)
        
        grads_and_var = list(zip(grads, var))
        # zip aggregate each gradient with parameters associated
        # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

        self.grads = grads
        self.var = var
        with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
            self.train_pi_op = self.trainer.apply_gradients(grads_and_var)
        
        value_params = tf.trainable_variables(self.name + '/main/q')
        # 2. Build our trainer
        with tf.control_dependencies([self.train_pi_op]):
            with tf.variable_scope(name + "_value", reuse=tf.AUTO_REUSE):
                self.value_trainer = MpiAdamOptimizer(comm, learning_rate=self.LR, mpi_rank_weight=mpi_rank_weight, epsilon=1e-5)

            # 3. Calculate the gradients
            with tf.variable_scope(name + "_value", reuse=tf.AUTO_REUSE):
                value_grads_and_var = self.value_trainer.compute_gradients(value_loss, value_params)
                value_grads, value_var = zip(*value_grads_and_var)
            
            value_grads, _value_grad_norm = tf.clip_by_global_norm(value_grads, max_grad_norm)
            
            value_grads_and_var = list(zip(value_grads, value_var))
            # zip aggregate each gradient with parameters associated
            # For instance zip(ABCD, xyza) => Ax, By, Cz, Da

            self.value_grads = value_grads
            self.value_var = value_var
            with tf.variable_scope(name + "_value", reuse=tf.AUTO_REUSE):
                self.train_value_op = self.value_trainer.apply_gradients(value_grads_and_var)

        # Polyak averaging for target variables
        # (control flow because sess.run otherwise evaluates in nondeterministic order)
        with tf.control_dependencies([self.train_value_op]):
            target_update = tf.group([tf.assign(v_targ, polyak*v_targ + (1-polyak)*v_main)
                                    for v_main, v_targ in zip(get_vars('main'), get_vars('target'))])

        # All ops to call during one training step
        self.loss_names = ["pi_loss", "q1_loss", "q2_loss", "LR"]
        self.stats_list = [pi_loss, q1_loss, q2_loss, self.LR, q1, q2, logp_pi, self.train_pi_op, self.train_value_op, target_update]
        
        # Initializing targets to match main variables
        self.target_init = tf.group([tf.assign(v_targ, v_main)
                                for v_main, v_targ in zip(get_vars('main'), get_vars('target'))])

        # self.init_buffer(size=int(1e6/self.rank))
        # self.init_buffer(size=int(1e6))
        self.init_buffer(size=int(1e5))

    def step(self, ob, im, stochastic=False):
        act_op = self.pi if stochastic else self.mu
        return self.sess.run(act_op, feed_dict={self.ob: ob.reshape(1,-1),self.im: im.reshape([1] + self.im_size)})[0]

    def run_train(self, ep_rets, ep_lens, evaluate=True):
        # TODO: Anneal LR

        # update_every = 50
        update_every = 1
        self.lr = self.learning_rate
        for _ in range(update_every):
            batch = self.sample_batch(self.batch_size)
            self.ob_rms.update(batch['obs1'])
            feed_dict = {self.ob: batch['obs1'],
                        self.im: batch['imgs1'],
                        self.ob2: batch['obs2'],
                        self.im2: batch['imgs2'],
                        self.ac: batch['acts'],
                        self.rew: batch['rews'],
                        self.done: batch['done'],
                        self.LR: self.lr}
            outs = self.sess.run(self.stats_list, feed_dict)
        self.loss = outs[:4]
        if evaluate:
            self.evaluate(ep_rets, ep_lens)
        
    def log_stuff(self, things):
        if self.rank == 0:
            for thing in things:
                self.writer.add_scalar(thing, things[thing], self.iters_so_far)
                logger.record_tabular(thing, things[thing])
        
    def init_buffer(self, size=int(1e6)):
        self.obs1_buf = np.zeros([size, self.ob_size], dtype=np.float32)
        self.obs2_buf = np.zeros([size, self.ob_size], dtype=np.float32)
        self.imgs1_buf = np.zeros([size] + self.im_size, dtype=np.float32)
        self.imgs2_buf = np.zeros([size] + self.im_size, dtype=np.float32)
        self.acts_buf = np.zeros([size, self.ac_size], dtype=np.float32)
        self.rews_buf = np.zeros(size, dtype=np.float32)
        self.done_buf = np.zeros(size, dtype=np.float32)
        self.ptr, self.size, self.max_size = 0, 0, size

    def add_to_buffer(self, data):
        # data = [obs, imgs, act, rew, next_obs, next_imgs, done]
        obs, imgs, act, rew, next_obs, next_imgs, done = data
        self.obs1_buf[self.ptr] = obs
        self.imgs1_buf[self.ptr] = imgs
        self.obs2_buf[self.ptr] = next_obs
        self.imgs2_buf[self.ptr] = next_imgs
        self.acts_buf[self.ptr] = act
        self.rews_buf[self.ptr] = rew
        self.done_buf[self.ptr] = done
        self.ptr = (self.ptr+1) % self.max_size
        self.size = min(self.size+1, self.max_size)

    def sample_batch(self, batch_size=32):
        idxs = np.random.randint(0, self.size, size=batch_size)
        return dict(obs1=self.obs1_buf[idxs],
                    obs2=self.obs2_buf[idxs],
                    imgs1=self.imgs1_buf[idxs],
                    imgs2=self.imgs2_buf[idxs],
                    acts=self.acts_buf[idxs],
                    rews=self.rews_buf[idxs],
                    done=self.done_buf[idxs])
